package pt.isel.ls;




import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;



public class TheaterDBTest {

    /*
    private final String DATABASE_NAME = "TESTDB";

    @Test
    public void testGetCinemasTheaters() {
        setDatabaseName(DATABASE_NAME);
        try {
            openConnection();
            getConnection().setAutoCommit(false);
            Cinema c1 = new Cinema("Colombo", "Lisboa");
            Theater t1 = new Theater("Sala 1", 200,10, 200/10,c1);
            Theater t2 = new Theater("Sala 2", 100,5, 100/5,c1);
            Theater t3 = new Theater("Sala 3", 100,5, 100/5,c1);
            int cid = CinemaDB.insertCinema(, c1);
            TheaterDB.insertTheater(, t1);
            TheaterDB.insertTheater(, t2);
            TheaterDB.insertTheater(, t3);
            List<Theater> expected = new ArrayList<>(Arrays.asList(t1, t2, t3));
            HashMap<String, String> values = new HashMap<>();
            values.put("{cid}", "" + cid);
            List<Theater> actual = TheaterDB.getCinemasTheaters(, Integer.parseInt(values.get("{cid}")));
            assertEquals(expected, actual);
            TheaterDB.deleteTheater(, t1.getId(), c1.getId());
            TheaterDB.deleteTheater(, t2.getId(), c1.getId());
            TheaterDB.deleteTheater(, t3.getId(), c1.getId());
            CinemaDB.deleteCinema(, c1.getId());
        }
        catch (DBException | SQLException e) {
            System.out.println("SQL ERROR: " + e.getMessage());
            if (getConnection() != null) {
                try {
                    getConnection().rollback();
                } catch (SQLException excep) {
                    System.out.println("SQL ERROR: " + excep.getMessage());
                }
            }
        }
        finally {
            try {
                getConnection().setAutoCommit(true);
                closeConnection();
            }
            catch (DBException | SQLException e) {
                System.out.println("SQL ERROR: " + e.getMessage());
            }
        }
    }

    @Test
    public void testGetCinemasTheatersID() {
        setDatabaseName(DATABASE_NAME);
        try {
            openConnection();
            getConnection().setAutoCommit(false);
            Cinema c = new Cinema("Colombo", "Lisboa");
            Theater expected = new Theater("Sala 1", 200,10, 200/10, c);
            int cid = CinemaDB.insertCinema(, c);
            int tid = TheaterDB.insertTheater(, expected);
            HashMap<String, String> values = new HashMap<>();
            values.put("{cid}", "" + cid);
            values.put("{tid}", "" + tid);
            Theater actual = (Theater)TheaterDB.getCinemasTheatersID(, values);
            assertEquals(expected, actual);
            TheaterDB.deleteTheater(, tid, cid);
            CinemaDB.deleteCinema(, cid);
        }
        catch (DBException | SQLException e) {
            System.out.println("SQL ERROR: " + e.getMessage());
            if (getConnection() != null) {
                try {
                    getConnection().rollback();
                } catch (SQLException excep) {
                    System.out.println("SQL ERROR: " + excep.getMessage());
                }
            }
        }
        finally {
            try {
                getConnection().setAutoCommit(true);
                closeConnection();
            }
            catch (DBException | SQLException e) {
                System.out.println("SQL ERROR: " + e.getMessage());
            }
        }
    }

    @Test
    public void testPostTheaters() {
        setDatabaseName(DATABASE_NAME);
        try {
            openConnection();
            getConnection().setAutoCommit(false);
            Cinema c = new Cinema("Colombo", "Lisboa");
            int cid = CinemaDB.insertCinema(, c);
            c.setId(cid);
            HashMap<String, String> values = new HashMap<>();
            values.put("{cid}", "" + cid);
            Theater expected = new Theater("Sala 1", 200,10, 200/10, c);
            Theater actual = TheaterDB.postTheaters(, values, "name=Sala+1&rows=10&seats=20");
            expected.setId(actual.getId());
            System.out.println(expected.equals(actual));
            assertEquals(expected, actual);
            TheaterDB.deleteTheater(, expected.getId(), cid);
            CinemaDB.deleteCinema(, cid);
        }
        catch (DBException | SQLException e) {
            System.out.println("SQL ERROR: " + e.getMessage());
            if (getConnection() != null) {
                try {
                    getConnection().rollback();
                } catch (SQLException excep) {
                    System.out.println("SQL ERROR: " + excep.getMessage());
                }
            }
        }
        finally {
            try {
                getConnection().setAutoCommit(true);
                closeConnection();
            }
            catch (DBException | SQLException e) {
                System.out.println("SQL ERROR: " + e.getMessage());
            }
        }
    }
    */
}
