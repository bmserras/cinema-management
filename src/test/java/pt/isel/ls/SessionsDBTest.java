package pt.isel.ls;



import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;



public class SessionsDBTest {

    /*
    private final String DATABASE_NAME = "TESTDB";

    @Test
    public void testGetCinemasSessions() {
        setDatabaseName(DATABASE_NAME);
        try {
            openConnection();
            getConnection().setAutoCommit(false);
            Cinema c = new Cinema("Colombo", "Lisboa");
            Movie m = new Movie("A Clockwork Orange", 1971, 136);
            Theater t1 = new Theater("Sala 1", 100, 5, 20, c);
            Theater t2 = new Theater("Sala 2", 50, 5, 10, c);
            Session s1 = new Session("2018-01-01,15:00", t1, m);
            Session s2 = new Session("2018-02-01,15:00", t1, m);
            Session s3 = new Session("2018-03-01,15:00", t2, m);
            int cid = CinemaDB.insertCinema(, c);
            int mid = MovieDB.insertMovie(, m);
            int tid1 = TheaterDB.insertTheater(, t1);
            int tid2 = TheaterDB.insertTheater(, t2);
            int sid1 = SessionDB.insertSession(, s1);
            int sid2 = SessionDB.insertSession(, s2);
            int sid3 = SessionDB.insertSession(, s3);
            HashMap<String,String> values = new HashMap<>();
            values.put("{cid}", "" + cid);
            List<Session> expected = new ArrayList<>(Arrays.asList(s1, s2, s3));
            List<Session> actual = SessionDB.getCinemasSessions(, values);
            assertIterableEquals(expected, actual);
            SessionDB.deleteSession(, sid1, tid1, cid);
            SessionDB.deleteSession(, sid2, tid1, cid);
            SessionDB.deleteSession(, sid3, tid2, cid);
            TheaterDB.deleteTheater(, tid1, cid);
            TheaterDB.deleteTheater(, tid2, cid);
            MovieDB.deleteMovie(, mid);
            CinemaDB.deleteCinema(, cid);
        }
        catch (DBException | SQLException e) {
            System.out.println("SQL ERROR: " + e.getMessage());
            if (getConnection() != null) {
                try {
                    getConnection().rollback();
                } catch (SQLException excep) {
                    System.out.println("SQL ERROR: " + excep.getMessage());
                }
            }
        }
        catch (SessionOverlapException e) {
            System.out.println("Session overlapping");
        }
        finally {
            try {
                getConnection().setAutoCommit(true);
                closeConnection();
            }
            catch (DBException | SQLException e) {
                System.out.println("SQL ERROR: " + e.getMessage());
            }
        }
    }

    @Test
    public void testGetCinemasSessionsID() {
        setDatabaseName(DATABASE_NAME);
        try {
            openConnection();
            getConnection().setAutoCommit(false);
            Cinema c = new Cinema("Colombo", "Lisboa");
            Movie m = new Movie("A Clockwork Orange",1971,136);
            Theater t = new Theater("Sala 1",100,5,20, c);
            Session expected = new Session("2018-01-01,15:00", t, m);
            int cid = CinemaDB.insertCinema(, c);
            int mid = MovieDB.insertMovie(, m);
            int tid = TheaterDB.insertTheater(, t);
            int sid =  SessionDB.insertSession(, expected);
            HashMap<String,String> values = new HashMap<>();
            values.put("{cid}", "" + cid);
            values.put("{sid}", "" + sid);
            Session actual = SessionDB.getCinemasSessionsID(, values);
            assertEquals(expected, actual);
            SessionDB.deleteSession(, sid, tid, cid);
            TheaterDB.deleteTheater(, tid, cid);
            CinemaDB.deleteCinema(, cid);
            MovieDB.deleteMovie(, mid);
        }
        catch (DBException | SQLException e) {
            System.out.println("SQL ERROR: " + e.getMessage());
            if (getConnection() != null) {
                try {
                    getConnection().rollback();
                } catch (SQLException excep) {
                    System.out.println("SQL ERROR: " + excep.getMessage());
                }
            }
        } catch (SessionOverlapException e) {
            System.out.println("Session overlapping");
        }
        finally {
            try {
                getConnection().setAutoCommit(true);
                closeConnection();
            }
            catch (DBException | SQLException e) {
                System.out.println("SQL ERROR: " + e.getMessage());
            }
        }
    }

    @Test
    public void testGetCinemasTheatersSessions() {
        setDatabaseName(DATABASE_NAME);
        try {
            openConnection();
            getConnection().setAutoCommit(false);
            Cinema c1 = new Cinema("Colombo", "Lisboa");
            int cinema_id = CinemaDB.insertCinema(, c1);
            Movie m1 = new Movie("A Clockwork Orange",1971,136);
            int mid1 = MovieDB.insertMovie(, m1);
            Theater t1 = new Theater("Sala 1",100,5,20,c1);
            Theater t2 = new Theater("Sala 2",50,5,10,c1);
            int tid1 = TheaterDB.insertTheater(, t1);
            int tid2 = TheaterDB.insertTheater(, t2);
            Session s1 = new Session("2018-01-01,15:00",t1,m1);
            Session s2 = new Session("2018-02-01,15:00",t1,m1);
            Session s3 = new Session("2018-03-01,15:00",t2,m1);
            int sid1 =  SessionDB.insertSession(, s1);
            int sid2 =  SessionDB.insertSession(, s2);
            int sid3 =  SessionDB.insertSession(, s3);
            HashMap<String,String> values = new HashMap<>();
            values.put("{cid}", "" + cinema_id);
            values.put("{tid}", "" + tid1);
            List<Session> expected = new ArrayList<>(Arrays.asList(s1, s2));
            List<Session> actual = SessionDB.getCinemasTheatersSessions(, values);
            assertIterableEquals(expected, actual);
            SessionDB.deleteSession(, sid1, tid1, cinema_id);
            SessionDB.deleteSession(, sid2, tid1, cinema_id);
            SessionDB.deleteSession(, sid3, tid2, cinema_id);
            TheaterDB.deleteTheater(, tid1, cinema_id);
            TheaterDB.deleteTheater(, tid2, cinema_id);
            MovieDB.deleteMovie(, mid1);
            CinemaDB.deleteCinema(, cinema_id);
        }
        catch (DBException | SQLException e) {
            System.out.println("SQL ERROR: " + e.getMessage());
            if (getConnection() != null) {
                try {
                    getConnection().rollback();
                } catch (SQLException excep) {
                    System.out.println("SQL ERROR: " + excep.getMessage());
                }
            }
        } catch (SessionOverlapException e) {

        } finally {
            try {
                getConnection().setAutoCommit(true);
                closeConnection();
            }
            catch (DBException | SQLException e) {
                System.out.println("SQL ERROR: " + e.getMessage());
            }
        }
    }

    @Test
    public void testGetCinemasSessionsToday() {
        setDatabaseName(DATABASE_NAME);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();
        String date = dtf.format(now) + ",00:00";
        try {
            openConnection();
            getConnection().setAutoCommit(false);
            Cinema c1 = new Cinema("Colombo", "Lisboa");
            int cinema_id = CinemaDB.insertCinema(, c1);
            Movie m1 = new Movie("A Clockwork Orange",1971,136);
            int mid1 = MovieDB.insertMovie(, m1);
            Theater t1 = new Theater("Sala 1",100,5,20,c1);
            Theater t2 = new Theater("Sala 2",50,5,10,c1);
            int tid1 = TheaterDB.insertTheater(, t1);
            int tid2 = TheaterDB.insertTheater(, t2);
            Session s1 = new Session(date,t1,m1);
            Session s2 = new Session(date,t2,m1);
            int sid1 = SessionDB.insertSession(, s1);
            int sid2 =SessionDB.insertSession(, s2);
            HashMap<String,String> values = new HashMap<>();
            values.put("{cid}", "" + cinema_id);
            List<Session> expected = new ArrayList<>(Arrays.asList(s1,s2));
            List<Session> actual = SessionDB.getCinemasSessionsToday(, values);
            assertEquals(expected, actual);
            SessionDB.deleteSession(, sid1, tid1, cinema_id);
            SessionDB.deleteSession(, sid2, tid2, cinema_id);
            TheaterDB.deleteTheater(, tid1, cinema_id);
            TheaterDB.deleteTheater(, tid2, cinema_id);
            CinemaDB.deleteCinema(, cinema_id);
            MovieDB.deleteMovie(, mid1);
        }
        catch (DBException | SQLException e) {
            System.out.println("SQL ERROR: " + e.getMessage());
            if (getConnection() != null) {
                try {
                    getConnection().rollback();
                } catch (SQLException excep) {
                    System.out.println("SQL ERROR: " + excep.getMessage());
                }
            }
        }
        catch (SessionOverlapException e) {
            System.out.println("Session overlapping");
        } finally {
            try {
                getConnection().setAutoCommit(true);
                closeConnection();
            }
            catch (DBException | SQLException e) {
                System.out.println("SQL ERROR: " + e.getMessage());
            }
        }
    }


    @Test
    public void testGetCinemasTheatersSessionsToday() {
        setDatabaseName(DATABASE_NAME);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();
        String date = dtf.format(now);
        try {
            openConnection();
            getConnection().setAutoCommit(false);
            Cinema c1 = new Cinema("Colombo", "Lisboa");
            int cinema_id = CinemaDB.insertCinema(, c1);
            Movie m1 = new Movie("A Clockwork Orange",1971,136);
            int mid1 = MovieDB.insertMovie(, m1);
            Theater t1 = new Theater("Sala 1",100,5,20,c1);
            Theater t2 = new Theater("Sala 2",50,5,10,c1);
            int tid1 = TheaterDB.insertTheater(, t1);
            int tid2 = TheaterDB.insertTheater(, t2);
            Session s1 = new Session(date + ",15:00",t1,m1);
            Session s2 = new Session(date + ",15:00",t2,m1);
            int sid1 = SessionDB.insertSession(, s1);
            int sid2 =SessionDB.insertSession(, s2);
            HashMap<String,String> values = new HashMap<>();
            values.put("{cid}", "" + cinema_id);
            values.put("{tid}", "" + tid2);
            List<Session> expected = new ArrayList<>(Arrays.asList(s2));
            List<Session> actual = SessionDB.getCinemasTheatersSessionsToday(, values);
            assertEquals(expected, actual);
            SessionDB.deleteSession(, sid1, tid1, cinema_id);
            SessionDB.deleteSession(, sid2, tid2, cinema_id);
            TheaterDB.deleteTheater(, tid1, cinema_id);
            TheaterDB.deleteTheater(, tid2, cinema_id);
            CinemaDB.deleteCinema(, cinema_id);
            MovieDB.deleteMovie(, mid1);
        }
        catch (DBException | SQLException e) {
            System.out.println("SQL ERROR: " + e.getMessage());
            if (getConnection() != null) {
                try {
                    getConnection().rollback();
                } catch (SQLException excep) {
                    System.out.println("SQL ERROR: " + excep.getMessage());
                }
            }
        }
        catch (SessionOverlapException e) {
            System.out.println("Session overlapping");
        } finally {
            try {
                getConnection().setAutoCommit(true);
                closeConnection();
            }
            catch (DBException | SQLException e) {
                System.out.println("SQL ERROR: " + e.getMessage());
            }
        }
    }


    @Test
    public void testGetCinemasSessionsDate() {
        setDatabaseName(DATABASE_NAME);
        try {
            openConnection();
            getConnection().setAutoCommit(false);
            Cinema c1 = new Cinema("Colombo", "Lisboa");
            int cinema_id = CinemaDB.insertCinema(, c1);
            Movie m1 = new Movie("A Clockwork Orange",1971,136);
            int mid1 = MovieDB.insertMovie(, m1);
            Theater t1 = new Theater("Sala 1",100,5,20,c1);
            Theater t2 = new Theater("Sala 2",50,5,10,c1);
            int tid1 = TheaterDB.insertTheater(, t1);
            int tid2 = TheaterDB.insertTheater(, t2);
            Session s1 = new Session("2018-01-01,15:00",t1,m1);
            Session s2 = new Session("2018-02-01,15:00",t1,m1);
            Session s3 = new Session("2018-01-01,18:00",t2,m1);
            int sid1 = SessionDB.insertSession(, s1);
            int sid2 =SessionDB.insertSession(, s2);
            int sid3 =SessionDB.insertSession(, s3);
            HashMap<String,String> values = new HashMap<>();
            values.put("{cid}", "" + cinema_id);
            values.put("{dmy}", "2018-01-01");
            List<Session> expected = new ArrayList<>(Arrays.asList(s1,s3));
            List<Session> actual = SessionDB.getCinemasSessionsDate(, values);
            assertIterableEquals(expected, actual);
            SessionDB.deleteSession(, sid1, tid1, cinema_id);
            SessionDB.deleteSession(, sid2, tid1, cinema_id);
            SessionDB.deleteSession(, sid3, tid2, cinema_id);
            TheaterDB.deleteTheater(, tid1, cinema_id);
            TheaterDB.deleteTheater(, tid2, cinema_id);
            MovieDB.deleteMovie(, mid1);
            CinemaDB.deleteCinema(, cinema_id);
        }
        catch (DBException | SQLException e) {
            System.out.println("SQL ERROR: " + e.getMessage());
            if (getConnection() != null) {
                try {
                    getConnection().rollback();
                } catch (SQLException excep) {
                    System.out.println("SQL ERROR: " + excep.getMessage());
                }
            }
        }
        catch (SessionOverlapException e) {
            System.out.println("Session overlapping");
        } finally {
            try {
                getConnection().setAutoCommit(true);
                closeConnection();
            }
            catch (SQLException | DBException e) {
                System.out.println("SQL ERROR: " + e.getMessage());
            }
        }
    }

    @Test
    public void testMoviesSessionsDate() {
        setDatabaseName(DATABASE_NAME);
        try {
            openConnection();
            getConnection().setAutoCommit(false);
            Cinema c1 = new Cinema("Colombo", "Lisboa");
            Cinema c2 = new Cinema("Forum Montijo", "Montijo");
            int cinema_id1 = CinemaDB.insertCinema(, c1);
            int cinema_id2 = CinemaDB.insertCinema(, c2);
            Movie m1 = new Movie("A Clockwork Orange",1971,136);
            int mid1 = MovieDB.insertMovie(, m1);
            Theater t1 = new Theater("Sala 1",100,5,20,c1);
            Theater t2 = new Theater("Sala 1",50,5,10,c2);
            int tid1 = TheaterDB.insertTheater(, t1);
            int tid2 = TheaterDB.insertTheater(, t2);
            Session s1 = new Session("2018-04-20,15:00",t1,m1);
            Session s2 = new Session("2018-02-01,20:00",t2,m1);
            Session s3 = new Session("2018-02-01,15:00",t1,m1);
            int sid1 = SessionDB.insertSession(, s1);
            int sid2 =SessionDB.insertSession(, s2);
            int sid3 =SessionDB.insertSession(, s3);
            HashMap<String,String> values = new HashMap<>();
            values.put("{mid}", "" + mid1);
            values.put("{dmy}", "2018-02-01");
            //No Parameters
            List<Session> expected1 = new ArrayList<>(Arrays.asList(s2,s3));
            List<Session> actual1 = SessionDB.getMoviesSessionsDate(, values, "");
            assertIterableEquals(expected1, actual1);
            //cid = 1;
            List<Session> expected2 = new ArrayList<>(Arrays.asList(s3));
            List<Session> actual2 = SessionDB.getMoviesSessionsDate(, values, "cid="+cinema_id1);
            assertIterableEquals(expected2, actual2);
            //city = "Montijo";
            List<Session> expected3 = new ArrayList<>(Arrays.asList(s2));
            List<Session> actual3 = SessionDB.getMoviesSessionsDate(, values, "city=Montijo");
            assertIterableEquals(expected3, actual3);
//            //available = 2;
            List<Session> expected4 = new ArrayList<>(Arrays.asList(s3));
            List<Session> actual4 = SessionDB.getMoviesSessionsDate(, values, "cid=1");
            assertIterableEquals(expected2, actual2);
            SessionDB.deleteSession(, sid1, tid1, cinema_id1);
            SessionDB.deleteSession(, sid2, tid2, cinema_id2);
            SessionDB.deleteSession(, sid3, tid1, cinema_id1);
            TheaterDB.deleteTheater(, tid1, cinema_id1);
            TheaterDB.deleteTheater(, tid2, cinema_id2);
            CinemaDB.deleteCinema(, cinema_id1);
            CinemaDB.deleteCinema(, cinema_id2);
            MovieDB.deleteMovie(, mid1);
        }
        catch (SQLException | DBException e) {
            System.out.println("SQL ERROR: " + e.getMessage());
            if (getConnection() != null) {
                try {
                    getConnection().rollback();
                } catch (SQLException excep) {
                    System.out.println("SQL ERROR: " + excep.getMessage());
                }
            }
        }
        catch (SessionOverlapException e) {
            System.out.println("Session overlapping");
        }
        finally {
            try {
                getConnection().setAutoCommit(true);
                closeConnection();
            }
            catch (SQLException | DBException e) {
                System.out.println("SQL ERROR: " + e.getMessage());
            }
        }
    }

    @Test
    public void testPostSessions() {
        setDatabaseName(DATABASE_NAME);
        try {
            openConnection();
            getConnection().setAutoCommit(false);
            Cinema c1 = new Cinema("Colombo", "Lisboa");
            int cinema_id = CinemaDB.insertCinema(, c1);
            Movie m1 = new Movie("A Clockwork Orange",1971,136);
            int mid1 = MovieDB.insertMovie(, m1);
            Theater t1 = new Theater("Sala 1",100,5,20,c1);
            int tid1 = TheaterDB.insertTheater(, t1);
            Session expected = new Session("2018-09-20,15:00",t1,m1);
            HashMap<String,String> values = new HashMap<>();
            values.put("{cid}", "" + cinema_id);
            values.put("{tid}", "" + tid1);
            Session actual = SessionDB.postSessions(, values, "date=2018-09-20,15:00&mid="+mid1);
            expected.setId(actual.getId());
            assertEquals(expected, actual);
            SessionDB.deleteSession(, expected.getId(), tid1, cinema_id);
            TheaterDB.deleteTheater(, tid1, cinema_id);
            CinemaDB.deleteCinema(, cinema_id);
            MovieDB.deleteMovie(, mid1);
        }
        catch (SQLException | DBException e) {
            System.out.println("SQL ERROR: " + e.getMessage());
            if (getConnection() != null) {
                try {
                    getConnection().rollback();
                } catch (SQLException excep) {
                    System.out.println("SQL ERROR: " + excep.getMessage());
                }
            }
        } finally {
            try {
                getConnection().setAutoCommit(true);
                closeConnection();
            }
            catch (SQLException | DBException e) {
                System.out.println("SQL ERROR: " + e.getMessage());
            }
        }
    }
    */
}
