CREATE DATABASE TESTDB;

USE TESTDB;

CREATE DATABASE LSDB;

USE LSDB;

SELECT * FROM CINEMA;
SELECT * FROM THEATER;
SELECT * FROM MOVIE;
SELECT * FROM SESSION;
SELECT * FROM TICKET;

DROP TABLE TICKET;
DROP TABLE SESSION;
DROP TABLE THEATER;
DROP TABLE MOVIE;
DROP TABLE CINEMA;

CREATE TABLE CINEMA (
	ID INT IDENTITY(1,1),
	Name VARCHAR(30),
	City VARCHAR(30),
	PRIMARY KEY (ID)
);

CREATE TABLE THEATER (
	ID INT IDENTITY(1,1),
	Name VARCHAR(30),
	NumberOfAvailableSeats INT,
	NumberOfRows INT,
	NumberOfSeatsPerRow INT,
	CinemaID INT,
	FOREIGN KEY (CinemaID) REFERENCES CINEMA(ID),
	PRIMARY KEY (ID, CinemaID)
);

CREATE TABLE MOVIE (
	ID INT,
	Title VARCHAR(200),
	ReleaseYear INT,
	Duration INT,
	PRIMARY KEY (ID)
);

CREATE TABLE SESSION (
	ID INT IDENTITY(1,1),
	DateTime VARCHAR(30),
	CinemaID INT,
	TheaterID INT,
	MovieID	INT,
	PRIMARY KEY (ID, TheaterID, MovieID),
	FOREIGN KEY (TheaterID, CinemaID) REFERENCES THEATER(ID, CinemaID),
	FOREIGN KEY (MovieID) REFERENCES MOVIE(ID)
);

CREATE TABLE TICKET (
	ID INT IDENTITY(1,1),
	Row VARCHAR(1),
	Seat INT,
	CinemaID INT,
	TheaterID INT,
	SessionID INT,
	MovieID INT,
	PRIMARY KEY (ID, SessionID, TheaterID, MovieID, Row, Seat),
	FOREIGN KEY (SessionID, TheaterID, MovieID) REFERENCES SESSION(ID, TheaterID, MovieID)
);

INSERT INTO MOVIE (ID, Title, ReleaseYear, Duration) VALUES (1, 'Shrek', 2001, 100);
INSERT INTO MOVIE (ID, Title, ReleaseYear, Duration) VALUES (2, 'Infinity War', 2018, 110);
INSERT INTO MOVIE (ID, Title, ReleaseYear, Duration) VALUES (3, 'Bee Movie', 2006, 90);

INSERT INTO CINEMA (Name, City) VALUES ('Colombo', 'Lisboa');
INSERT INTO THEATER (Name, NumberOfAvailableSeats, NumberOfRows, NumberOfSeatsPerRow, CinemaID) VALUES ('Sala 1', 200, 10, 20, 1);
INSERT INTO THEATER (Name, NumberOfAvailableSeats, NumberOfRows, NumberOfSeatsPerRow, CinemaID) VALUES ('Sala 2', 150, 15, 10, 1);
INSERT INTO THEATER (Name, NumberOfAvailableSeats, NumberOfRows, NumberOfSeatsPerRow, CinemaID) VALUES ('Sala 3', 120, 12, 10, 1);

INSERT INTO CINEMA (Name, City) VALUES ('Forum', 'Sintra');
INSERT INTO THEATER (Name, NumberOfAvailableSeats, NumberOfRows, NumberOfSeatsPerRow, CinemaID) VALUES ('Sala 1', 180, 18, 10, 2);
INSERT INTO THEATER (Name, NumberOfAvailableSeats, NumberOfRows, NumberOfSeatsPerRow, CinemaID) VALUES ('Sala 2', 140, 14, 10, 2);
INSERT INTO THEATER (Name, NumberOfAvailableSeats, NumberOfRows, NumberOfSeatsPerRow, CinemaID) VALUES ('Sala 3', 100, 10, 10, 2);

INSERT INTO CINEMA (Name, City) VALUES ('Amoreiras', 'Lisboa');
INSERT INTO CINEMA (Name, City) VALUES ('Vasco da Gama', 'Lisboa');

INSERT INTO SESSION (DateTime, CinemaID, TheaterID, MovieID) VALUES ('2018-05-28,10:00', 1, 1, 1);
INSERT INTO SESSION (DateTime, CinemaID, TheaterID, MovieID) VALUES ('2018-05-28,10:00', 2, 4, 2);
INSERT INTO SESSION (DateTime, CinemaID, TheaterID, MovieID) VALUES ('2018-05-29,11:00', 1, 3, 3);
INSERT INTO SESSION (DateTime, CinemaID, TheaterID, MovieID) VALUES ('2018-05-29,11:00', 1, 3, 2);
INSERT INTO SESSION (DateTime, CinemaID, TheaterID, MovieID) VALUES ('2018-05-29,12:00', 2, 4, 1);
INSERT INTO SESSION (DateTime, CinemaID, TheaterID, MovieID) VALUES ('2018-05-29,13:00', 1, 2, 1);
INSERT INTO SESSION (DateTime, CinemaID, TheaterID, MovieID) VALUES ('2018-05-29,15:00', 1, 2, 3);
INSERT INTO SESSION (DateTime, CinemaID, TheaterID, MovieID) VALUES ('2018-05-29,16:00', 2, 5, 3);
INSERT INTO SESSION (DateTime, CinemaID, TheaterID, MovieID) VALUES ('2018-05-30,16:00', 2, 5, 1);
INSERT INTO SESSION (DateTime, CinemaID, TheaterID, MovieID) VALUES ('2018-05-30,17:00', 2, 6, 2);
INSERT INTO SESSION (DateTime, CinemaID, TheaterID, MovieID) VALUES ('2018-05-31,20:00', 1, 1, 1);
INSERT INTO SESSION (DateTime, CinemaID, TheaterID, MovieID) VALUES ('2018-05-28,20:00', 1, 1, 1);

INSERT INTO TICKET (Row, Seat, CinemaID, TheaterID, SessionID, MovieID) VALUES ('A', 1, 1, 1, 1, 1);
INSERT INTO TICKET (Row, Seat, CinemaID, TheaterID, SessionID, MovieID) VALUES ('A', 2, 1, 1, 1, 1);
INSERT INTO TICKET (Row, Seat, CinemaID, TheaterID, SessionID, MovieID) VALUES ('A', 3, 1, 1, 1, 1);
INSERT INTO TICKET (Row, Seat, CinemaID, TheaterID, SessionID, MovieID) VALUES ('A', 4, 1, 1, 1, 1);
INSERT INTO TICKET (Row, Seat, CinemaID, TheaterID, SessionID, MovieID) VALUES ('A', 5, 1, 1, 1, 1);
INSERT INTO TICKET (Row, Seat, CinemaID, TheaterID, SessionID, MovieID) VALUES ('A', 6, 1, 1, 1, 1);