package pt.isel.ls.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.isel.ls.commands.Command;
import pt.isel.ls.exceptions.*;
import pt.isel.ls.headers.Header;
import pt.isel.ls.headers.HeaderManager;
import pt.isel.ls.results.Result;
import pt.isel.ls.utils.Pair;
import pt.isel.ls.view.View;
import pt.isel.ls.view.html.ViewHtmlError;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import static pt.isel.ls.commands.CommandManager.getCommandAndValues;

public class AppServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(AppServlet.class);

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String request = req.getRequestURI();
        try {
            String method = req.getMethod();
            if (request.equals("/favicon.ico")) {
                return;
            }
            String header = req.getHeader("Accept");
            if (header.contains(",")){
                header = header.substring(0,header.indexOf(","));
            }
            logger.info("{} on '{}' with accept:'{}', instance {}",
                    method, request, header,
                    this.hashCode());
            Pair<Command, HashMap<String, String>> pair = getCommandAndValues(method, request);
            Command command = pair.getFirst();
            HashMap<String, String> pathValues = pair.getSecond();
            Result rs = command.execute(pathValues, "");
            Charset utf8 = Charset.forName("utf-8");
            Header headerH = HeaderManager.decodeHeader("accept:" + header);
            View v = View.getView(rs, headerH);
            String s = v.getContent();
            header += "; charset=%s";
            resp.setContentType(String.format(header, utf8.name()));
            byte[] respBodyBytes = s.getBytes(utf8);
            resp.setStatus(HttpStatusCode.Ok.valueOf());
            resp.setContentLength(respBodyBytes.length);
            OutputStream os = resp.getOutputStream();
            os.write(respBodyBytes);
            os.close();
            logger.info("Response sent");
        } catch (CommandNotAvailableException e) {
            resp.setStatus(HttpStatusCode.BadRequest.valueOf());
            logger.info("Unexpected CommandNotAvailableException while handling the request",e);
            System.out.println(e.toString());
        } catch (InternalErrorException e) {
            resp.setStatus(HttpStatusCode.InternalServerError.valueOf());
            logger.info("Unexpected InternalErrorException while handling the request",e);
            System.out.println(e.toString());
        } catch (DBException e){
            resp.setStatus(HttpStatusCode.NotFound.valueOf());
            logger.info(e.getMessage());
            Charset utf8 = Charset.forName("utf-8");
            int i = request.lastIndexOf("/");
            ViewHtmlError v = new ViewHtmlError(e.getMessage(), request.substring(0, i));
            try {
                v.html();
            }
            catch (InternalErrorException ei) {
                logger.info(e.getMessage());
            }
            String s = v.getContent();
            String header = "text/html; charset=%s";
            resp.setContentType(String.format(header, utf8.name()));
            byte[] respBodyBytes = s.getBytes(utf8);
            resp.setContentLength(respBodyBytes.length);
            OutputStream os = resp.getOutputStream();
            os.write(respBodyBytes);
            os.close();
            logger.info("Response sent");
            System.out.println(e.toString());
        }
        catch(Exception e) {
            logger.info("Unexpected Exception while handling the request",e);
            System.out.println(e.toString());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String request=req.getRequestURI();
        try {
            String method = req.getMethod();
            String header = req.getHeader("Accept");
            if (header.contains(",")){
                header = header.substring(0,header.indexOf(","));
            }
            logger.info("{} on '{}' with accept:'{}', instance {}",
                    method, req, header,
                    this.hashCode());
            Map<String, String[]> parameters = req.getParameterMap();
            StringBuilder params = new StringBuilder();
            for(String parameter : parameters.keySet()) {
                if (!parameter.equals("Submit")) params.append(parameter).append("=").append(parameters.get(parameter)[0]).append("&");
            }
            params.deleteCharAt(params.length()-1);
            logger.info("Parameters: " + params.toString());
            Pair<Command, HashMap<String, String>> pair = getCommandAndValues(method, request);
            Command command = pair.getFirst();
            HashMap<String, String> pathValues = pair.getSecond();
            Result rs = command.execute(pathValues, params.toString());
            logger.info("Post done");
            String link = request + "/" + rs.getEntity().getId();
            HttpResponse hr = new HttpResponse(HttpStatusCode.SeeOther).withHeader("Location", link);
            logger.info("Response sent");
            hr.send(resp);
        }
        catch (CommandNotAvailableException e) {
            resp.setStatus(HttpStatusCode.BadRequest.valueOf());
            logger.info("Unexpected CommandNotAvailableException while handling the request",e);
            System.out.println(e.toString());
        } catch (InternalErrorException e) {
            resp.setStatus(HttpStatusCode.InternalServerError.valueOf());
            logger.info("Unexpected InternalErrorException while handling the request",e);
            System.out.println(e.toString());
        }
        catch (WrongParametersException | SessionOverlapException | NotBetweenAandZException | AlreadyExistsRowSeatException | TicketNotAvailableException | DBException e) {
            resp.setStatus(HttpStatusCode.NotFound.valueOf());
            logger.info(e.getMessage());
            Charset utf8 = Charset.forName("utf-8");
            int i = request.lastIndexOf("/");
            ViewHtmlError v = new ViewHtmlError(e.getMessage(), request.substring(0, i));
            try {
                v.html();
            }
            catch (InternalErrorException ei) {
                logger.info(e.getMessage());
            }
            String s = v.getContent();
            String header = "text/html; charset=%s";
            resp.setContentType(String.format(header, utf8.name()));
            byte[] respBodyBytes = s.getBytes(utf8);
            resp.setContentLength(respBodyBytes.length);
            OutputStream os = resp.getOutputStream();
            os.write(respBodyBytes);
            os.close();
            logger.info("Response sent");
            System.out.println(e.toString());
        }
        catch(Exception e) {
            logger.info("Unexpected Exception while handling the request",e);
            System.out.println(e.toString());
        }

    }
}