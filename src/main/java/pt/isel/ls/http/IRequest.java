package pt.isel.ls.http;

import pt.isel.ls.exceptions.DBException;

import java.io.InputStream;
import java.util.function.Consumer;

public interface IRequest {
    InputStream getBody(String path) throws DBException;

    public default IRequest compose(Consumer<String> cons) {
        return path -> {
            cons.accept(path);
            return getBody(path);
        };
    }
}
