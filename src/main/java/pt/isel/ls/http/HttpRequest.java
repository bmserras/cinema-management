package pt.isel.ls.http;

import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.exceptions.InternalErrorException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class HttpRequest implements IRequest {

    @Override
    public InputStream getBody(String path) throws DBException {
        try {
            System.out.println(path);
            return new URL(path).openStream();
        } catch (IOException e) {
            throw new DBException("Item not found", e);
        }
    }
}