package pt.isel.ls.results;

import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Movie;
import pt.isel.ls.entities.Theater;

import java.util.List;

public class ResultCinemasID extends Result {

    private Cinema cinema;

    private List<Theater> theaters;

    private List<Movie> movies;

    public ResultCinemasID(Cinema cinema, List<Theater> theaters, List<Movie> movies) {
        super(cinema);
        this.cinema = cinema;
        this.theaters = theaters;
        this.movies = movies;
    }

    public Cinema getCinema() {
        return cinema;
    }

    public void setCinema(Cinema cinema) {
        this.cinema = cinema;
    }

    public List<Theater> getTheaters() {
        return theaters;
    }

    public List<Movie> getMovies() {
        return movies;
    }

}
