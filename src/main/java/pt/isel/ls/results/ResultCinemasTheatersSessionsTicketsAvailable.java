package pt.isel.ls.results;

import pt.isel.ls.entities.Session;
import pt.isel.ls.utils.Pair;

public class ResultCinemasTheatersSessionsTicketsAvailable extends Result {

    private Session session;
    private int ticketsAvailable;

    public ResultCinemasTheatersSessionsTicketsAvailable(Session session, int ticketsAvailable) {
        super(session);
        this.session = session;
        this.ticketsAvailable = ticketsAvailable;
        getDetailedList().add(new Pair<>("Number of tickets available: ", "" + (session.getTheater().getSeats() - ticketsAvailable)));
    }

    public Session getSession() {
        return session;
    }

    public int getTicketsAvailable() {
        return ticketsAvailable;
    }



}
