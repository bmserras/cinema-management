package pt.isel.ls.results;

import pt.isel.ls.entities.Cinema;

import java.util.List;

public class ResultCinemas extends Result {

    private List<Cinema> cinemas;

    public ResultCinemas(List<Cinema> cinemas) {
        super(cinemas);
        this.cinemas = cinemas;
    }

    public List<Cinema> getCinemas() {
        return cinemas;
    }

}
