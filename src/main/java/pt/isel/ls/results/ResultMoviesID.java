package pt.isel.ls.results;

import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Movie;
import pt.isel.ls.entities.Session;

import java.util.List;

public class ResultMoviesID extends Result{

    private Movie movie;
    private List<Session> sessions;
    private List<Cinema> cinemas;

    public ResultMoviesID(Movie movie, List<Cinema> cinemas, List<Session> sessions) {
        super(movie);
        this.movie = movie;
        this.cinemas = cinemas;
        this.sessions = sessions;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public List<Cinema> getCinemas() {
        return cinemas;
    }

    public void setCinemas(List<Cinema> cinemas) {
        this.cinemas = cinemas;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

}
