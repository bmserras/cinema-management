package pt.isel.ls.results;

import pt.isel.ls.entities.Ticket;

import java.util.List;

public class ResultCinemasTheatersSessionsTickets extends Result{
    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    private List<Ticket> tickets;

    public ResultCinemasTheatersSessionsTickets(List<Ticket> tickets) {
        super(tickets);
        this.tickets = tickets;
    }

}
