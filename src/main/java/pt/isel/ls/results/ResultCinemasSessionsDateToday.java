package pt.isel.ls.results;

import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Session;

import java.time.LocalDateTime;
import java.util.List;

public class ResultCinemasSessionsDateToday extends Result {

    private List<Session> sessions;

    private Cinema cinema;

    private LocalDateTime localDateTime;

    public ResultCinemasSessionsDateToday(List<Session> sessions, Cinema cinema, LocalDateTime localDateTime) {
        super(sessions);
        this.sessions = sessions;
        this.cinema = cinema;
        this.localDateTime = localDateTime;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    public Cinema getCinema() {
        return cinema;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }
}
