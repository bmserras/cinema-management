package pt.isel.ls.results;

import pt.isel.ls.entities.Session;

import java.util.List;

public class ResultCinemasTheatersSessionsToday extends Result{
    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    private List<Session> sessions;

    public ResultCinemasTheatersSessionsToday(List<Session> sessions) {
        super(sessions);
        this.sessions = sessions;
    }

}
