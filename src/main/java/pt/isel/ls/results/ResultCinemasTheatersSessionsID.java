package pt.isel.ls.results;

import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Session;
import pt.isel.ls.entities.Theater;
import pt.isel.ls.entities.Ticket;

import java.util.List;

public class ResultCinemasTheatersSessionsID extends Result {

    private Cinema cinema;
    private Theater theater;
    private List<Ticket> tickets;
    private Session session;

    public Cinema getCinema() {
        return cinema;
    }

    public Theater getTheater() {
        return theater;
    }

    public ResultCinemasTheatersSessionsID(Cinema cinema, Theater theater, List<Ticket> tickets, Session session) {
        super(session);
        this.cinema = cinema;
        this.theater = theater;
        this.tickets = tickets;
        this.session = session;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setSessions(Session sessions) {
        this.session = sessions;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(List<Ticket> tickets) {
        this.tickets = tickets;
    }

}
