package pt.isel.ls.results;

import pt.isel.ls.entities.Session;
import pt.isel.ls.entities.Ticket;

public class ResultCinemasTheatersSessionsTicketsID extends Result {

    private Session session;

    public Session getSession() {
        return session;
    }

    private Ticket ticket;

    public ResultCinemasTheatersSessionsTicketsID(Ticket ticket, Session session) {
        super(ticket);
        this.ticket = ticket;
        this.session = session;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

}
