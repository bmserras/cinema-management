package pt.isel.ls.results;

import pt.isel.ls.entities.Movie;

import java.util.List;

public class ResultMovies extends Result {

    private List<Movie> movies;

    public ResultMovies(List<Movie> movies) {
        super(movies);
        this.movies = movies;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

}
