package pt.isel.ls.results;

import pt.isel.ls.entities.Session;

import java.util.List;

public class ResultCinemasSessions extends Result {

    private List<Session> sessions;

    public ResultCinemasSessions(List<Session> sessions) {
        super(sessions);
        this.sessions = sessions;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setCinemas(List<Session> sessions) {
        this.sessions = sessions;
    }

}
