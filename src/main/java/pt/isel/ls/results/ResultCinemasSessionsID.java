package pt.isel.ls.results;

import pt.isel.ls.entities.Session;

public class ResultCinemasSessionsID extends Result {

    private Session session;

    public ResultCinemasSessionsID(Session session) {
        super(session);
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    public void setCinemas(Session session) {
        this.session = session;
    }

}
