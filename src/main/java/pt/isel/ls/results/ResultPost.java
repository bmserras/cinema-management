package pt.isel.ls.results;

import pt.isel.ls.entities.Entity;
import pt.isel.ls.commands.NoHeaderRequired;

public class ResultPost extends Result implements NoHeaderRequired {
    private Entity entity;

    public ResultPost(Entity entity) {
        super(entity);
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }
}