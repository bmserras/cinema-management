package pt.isel.ls.results;

import pt.isel.ls.entities.Session;

import java.util.List;

public class ResultMoviesSessionsDate extends Result {

    private List<Session> sessions;

    public ResultMoviesSessionsDate(List<Session> sessions) {
        super(sessions);
        this.sessions = sessions;
    }

    public List<Session> getSessions() {
        return sessions;
    }

}
