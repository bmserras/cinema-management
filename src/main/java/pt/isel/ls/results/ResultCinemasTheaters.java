package pt.isel.ls.results;

import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Theater;

import java.util.List;

public class ResultCinemasTheaters extends Result {

    private List<Theater> theaters;

    public Cinema getCinema() {
        return cinema;
    }

    private Cinema cinema;

    public ResultCinemasTheaters(List<Theater> theaters, Cinema cinema) {
        super(theaters);
        this.theaters = theaters;
        this.cinema = cinema;
    }

    public List<Theater> getTheaters() {
        return theaters;
    }

    public void setTheaters(List<Theater> theaters) {
        this.theaters = theaters;
    }

}
