package pt.isel.ls.results;

import pt.isel.ls.entities.Session;
import pt.isel.ls.entities.Theater;

import java.util.List;

public class ResultCinemasTheatersID extends Result {

    private List<Session> sessions;
    private Theater theater;

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    public Theater getTheater() {
        return theater;
    }

    public void setTheater(Theater theater) {
        this.theater = theater;
    }

    public ResultCinemasTheatersID(List<Session> sessions, Theater theater) {
        super(theater);
        this.sessions = sessions;
        this.theater = theater;
    }

}
