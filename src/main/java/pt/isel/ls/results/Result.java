package pt.isel.ls.results;

import pt.isel.ls.entities.Entity;
import pt.isel.ls.utils.Pair;

import java.util.List;

public class Result {

    private Entity entity;

    private List<? extends Entity> list;

    private List<Pair<String, String>> detailedList;

    public Result() {

    }

    public Result(Entity entity) {
        this.entity = entity;
        this.detailedList = entity.toListDetailed();
    }

    public Result(List<? extends Entity> list) {
        this.list = list;
    }


    public List<? extends Entity> getList() {
        return list;
    }

    public Entity getEntity() {
        return entity;
    }

    public List<Pair<String, String>> getDetailedList() {
        return detailedList;
    }
}
