package pt.isel.ls.results;

import pt.isel.ls.entities.Entity;
import pt.isel.ls.utils.Pair;

import java.util.List;

public class ResultOption extends Result {

    private List<Pair<String,String>> options;

    public ResultOption(List<Pair<String,String>> options) {
        this.options = options;
    }

    public List<Pair<String, String>> getOptions() {
        return options;
    }

}
