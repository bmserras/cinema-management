package pt.isel.ls.html;

import pt.isel.ls.common.Writable;

import java.io.IOException;
import java.io.Writer;

public class HtmlTag implements Writable {

    private final String _name;

    public HtmlTag(String name) {
        _name = name;
    }

    @Override
    public void writeTo(Writer w) throws IOException {
        w.write(String.format("<%s>", _name));
    }

}
