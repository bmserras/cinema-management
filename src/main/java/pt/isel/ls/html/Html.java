package pt.isel.ls.html;

import org.omg.CORBA.WrongTransaction;
import pt.isel.ls.common.CompositeWritable;
import pt.isel.ls.common.Writable;
import pt.isel.ls.http.HttpContent;

import java.io.IOException;
import java.io.Writer;

public class Html implements HttpContent {

    private Writable _content;

    public Html(Writable... cs) {
        _content = new CompositeWritable(cs);
    }

    @Override
    public void writeTo(Writer w) throws IOException {
        _content.writeTo(w);
    }

    @Override
    public String getMediaType() {
        return "text/html";
    }

    public static Writable text(String s) { return new HtmlText(s);}

    public static Writable h1(Writable... c) { return new HtmlElem("h1",c);}
    public static Writable h2(Writable... c) { return new HtmlElem("h2",c);}
    public static Writable h3(Writable... c) { return new HtmlElem("h3",c);}

    public static Writable form(String method, String url, Writable... c) {
        return new HtmlElem("form",c)
                .withAttr("method", method)
                .withAttr("action", url);
    }

    public static Writable label(String to, String text) {
        return new HtmlElem("label", new HtmlText(text))
                .withAttr("for", to);
    }


    public static Writable textInput(String name) {
        return new HtmlElem("input")
                .withAttr("type", "text")
                .withAttr("name", name);
    }

    public static Writable buttonSubmit(String name) {
        return new HtmlElem("input")
                .withAttr("type", "submit")
                .withAttr("name", name);
    }

    public static Writable ul(Writable... c) {
        return new HtmlElem("ul",c);
    }

    public static Writable li(Writable... c) {
        return new HtmlElem("li",c);
    }

    public static Writable a(String href, String t) {
        return new HtmlElem("a", text(t))
                .withAttr("href", href);
    }

    public static Writable aImg(String href, String t) {
        return new HtmlElem("a", img(t))
                .withAttr("href", href);
    }

    public static Writable img(String src) {
        return new HtmlTag("img src=" + src);
    }

    public static Writable table(Writable... c) {
        return new HtmlElem("table",c);
    }

    public static Writable style() {
        return new HtmlElem(
                "style",
                text("table { width:100%; } " +
                "table, th, td { border: 1px solid black; border-collapse: collapse; } " +
                "th, td { padding: 15px; text-align: left; } " +
                "table tr:nth-child(even) { background-color: #eee; } " +
                "table tr:nth-child(odd) { background-color: #fff; } " +
                "table th { background-color: black; color: white; }"
                )
        );
    }

    public static Writable p(Writable... c) { return new HtmlElem("p",c); }

    public static Writable m(Writable... c) { return new HtmlElem("marquee",c); }

    public static Writable tr(Writable... c) { return new HtmlElem("tr",c); }

    public static Writable th(Writable... c) { return new HtmlElem("th",c); }

    public static Writable td(Writable... c) { return new HtmlElem("td",c); }

    public static Writable br() { return new HtmlTag("br"); }

    public static Writable hr() { return new HtmlTag("hr"); }

    public static Writable small(Writable... c) { return new HtmlElem("small", c); };

}