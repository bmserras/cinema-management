package pt.isel.ls.exceptions;

public class UnknownHeaderException extends Exception {
    public UnknownHeaderException(String msg) { super (msg); }
}
