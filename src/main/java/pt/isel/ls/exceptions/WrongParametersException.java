package pt.isel.ls.exceptions;

public class WrongParametersException extends Exception {

    public WrongParametersException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public WrongParametersException(String msg) {
        super(msg);
    }

}
