package pt.isel.ls.exceptions;

public class AlreadyExistsRowSeatException extends Exception {
    public AlreadyExistsRowSeatException(String msg) {
        super(msg);
    }
}