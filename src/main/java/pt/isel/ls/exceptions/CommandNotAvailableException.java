package pt.isel.ls.exceptions;

public class CommandNotAvailableException extends Exception {
    public CommandNotAvailableException(String msg) {
        super(msg);
    }
}
