package pt.isel.ls.exceptions;

public class InternalErrorException extends Exception {
    public InternalErrorException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
