package pt.isel.ls.exceptions;

public class NotBetweenAandZException extends Exception {
    public NotBetweenAandZException(String msg) {
        super(msg);
    }
}