package pt.isel.ls.exceptions;

public class DBException extends Exception {
    public DBException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
