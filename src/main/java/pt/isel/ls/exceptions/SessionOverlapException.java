package pt.isel.ls.exceptions;

public class SessionOverlapException extends Exception {
    public SessionOverlapException(String msg) {
        super(msg);
    }
}
