package pt.isel.ls.headers;

public class Header {
    private boolean isFile;
    private String fileName;

    public boolean isFile() {
        return isFile;
    }

    public void setFile(boolean file) {
        isFile = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getName(){
        return "";
    }

}
