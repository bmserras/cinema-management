package pt.isel.ls.headers;

import java.util.HashMap;

public class HeaderManager {

    private static HashMap<String, Header> headerMap;

    public static void addHeaders() {
        headerMap = new HashMap<>();
        headerMap.put("", new TextHtml());
        headerMap.put("text/html", new TextHtml());
        headerMap.put("text/plain", new TextPlain());
        headerMap.put("application/json", new ApplicationJson());
    }

    public static Header decodeHeader(String header) {
        String headers[] = header.split("[|]");
        boolean accept = false, file = false;
        String msg = "", name = "";
        if (headers[0].isEmpty()) { // se não houver header
            msg = "text/html"; // por default
        }
        else {
            for (String s : headers) {
                String[] arr = s.split(":");
                if (arr.length != 2) { // se o formato não for aaa:bbb
                    return null;
                }
                if (arr[0].equals("accept")) {
                    if (accept) return null; // há mais que um header de accept
                    accept = true;
                    switch (arr[1]) {
                        case "text/plain":
                            msg = "text/plain";
                            break;
                        case "text/html":
                            msg = "text/html";
                            break;
                        case "application/json":
                            msg = "application/json";
                            break;
                        default:
                            return null;
                    }
                } else if (arr[0].equals("file-name")) {
                    if (file) return null; // há mais que um header de file-name
                    file = true;
                    name = arr[1];
                }
            }
            if (!accept) msg = "text/html";
        }
        Header h = getHeader(msg);
        h.setFile(file);
        h.setFileName(name);
        return h;
    }

    private static Header getHeader(String header) {
        return headerMap.get(header);
    }

}
