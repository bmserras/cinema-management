package pt.isel.ls.view.json;

import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultOption;
import pt.isel.ls.utils.Pair;

import java.util.List;


public class ViewJsonDetailed extends ViewJson {

    private String content;
    private String title;

    public ViewJsonDetailed(String title) {
        this.title = title;
    }

    @Override
    public ViewJson json(Result res) {
        String str = ("{");
        str += "\"" + title + "\":";
        StringBuilder json = new StringBuilder(str);
        json.append("{");
        List<Pair<String,String>> list;
        if(res instanceof ResultOption){
            list = ((ResultOption) res).getOptions();
        }else{
            list = res.getDetailedList();
        }

        for (Pair p : list) {
            String s = "\"" + p.getFirst() + "\"" + " : " + "\"" + p.getSecond() + "\" ,";
            json.append(s);
        }

        json.deleteCharAt(json.length()-1);
        json.append("}}");
        content = json.toString();
        return this;
    }

    @Override
    public String getContent() {
        return content;
    }
}
