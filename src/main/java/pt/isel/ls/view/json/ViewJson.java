package pt.isel.ls.view.json;

import pt.isel.ls.results.*;
import pt.isel.ls.view.View;

import java.util.HashMap;

public abstract class ViewJson extends View {

    public abstract ViewJson json(Result res);

    public static HashMap<String, ViewJson> jsonMap = new HashMap<>();

    public static void addViews() {

        jsonMap.put(ResultCinemas.class.getName(), new ViewJsonNotDetailed("CINEMAS"));
        jsonMap.put(ResultCinemasID.class.getName(), new ViewJsonDetailed("CINEMA"));

        jsonMap.put(ResultMovies.class.getName(), new ViewJsonNotDetailed("MOVIES"));
        jsonMap.put(ResultMoviesID.class.getName(), new ViewJsonDetailed("MOVIE"));

        jsonMap.put(ResultCinemasTheaters.class.getName(), new ViewJsonNotDetailed("THEATERS"));
        jsonMap.put(ResultCinemasTheatersID.class.getName(), new ViewJsonDetailed("THEATER"));

        jsonMap.put(ResultCinemasTheatersSessions.class.getName(), new ViewJsonNotDetailed("SESSIONS"));
        jsonMap.put(ResultCinemasTheatersSessionsID.class.getName(), new ViewJsonDetailed("SESSION"));

        jsonMap.put(ResultCinemasTheatersSessionsTickets.class.getName(), new ViewJsonNotDetailed("TICKETS"));
        jsonMap.put(ResultCinemasTheatersSessionsTicketsID.class.getName(), new ViewJsonDetailed("TICKET"));

        jsonMap.put(ResultCinemasSessions.class.getName(), new ViewJsonNotDetailed("SESSIONS"));
        jsonMap.put(ResultCinemasSessionsID.class.getName(), new ViewJsonDetailed("SESSION"));

        jsonMap.put(ResultCinemasSessionsDate.class.getName(), new ViewJsonNotDetailed("SESSIONS"));
        jsonMap.put(ResultCinemasSessionsDateToday.class.getName(), new ViewJsonNotDetailed("SESSIONS"));
        jsonMap.put(ResultCinemasTheatersSessionsToday.class.getName(), new ViewJsonNotDetailed("SESSIONS"));
        jsonMap.put(ResultMoviesSessionsDate.class.getName(), new ViewJsonNotDetailed("SESSIONS"));

        jsonMap.put(ResultCinemasTheatersSessionsTicketsAvailable.class.getName(), new ViewJsonDetailed("SESSION"));
        jsonMap.put(ResultOption.class.getName(), new ViewJsonDetailed("OPTIONS"));


    }

}
