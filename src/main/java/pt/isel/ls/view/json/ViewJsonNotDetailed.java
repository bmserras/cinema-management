package pt.isel.ls.view.json;

import pt.isel.ls.entities.Entity;
import pt.isel.ls.results.Result;
import pt.isel.ls.utils.Pair;

import java.util.List;

public class ViewJsonNotDetailed extends ViewJson {

    private String content;
    private String title;

    public ViewJsonNotDetailed(String title) {
        this.title = title;
    }

    @Override
    public ViewJson json(Result res) {
        String str = ("{");
        str += "\"" + title + "\":[";
        StringBuilder json = new StringBuilder(str);
        for (Entity e : res.getList()) {
            List<Pair<String, String>> pairs = e.toListDate();
            json.append("{");
            for (Pair pair : pairs) {
                String s = "\"" + pair.getFirst() + "\"" + " : " + "\"" + pair.getSecond() + "\" ,";
                json.append(s);
            }
            json.deleteCharAt(json.length()-1);
            json.append("}, ");
        }
        json.deleteCharAt(json.length()-2);
        json.append("]}");
        content = json.toString();
        return this;
    }

    @Override
    public String getContent() {
        return content;
    }
}
