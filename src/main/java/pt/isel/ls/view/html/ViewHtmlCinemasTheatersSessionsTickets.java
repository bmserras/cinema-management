package pt.isel.ls.view.html;

import pt.isel.ls.entities.Ticket;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.Html;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasTheatersSessionsTickets;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

public class ViewHtmlCinemasTheatersSessionsTickets extends ViewHtml {

    String content;

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public ViewHtml html(Result res) throws InternalErrorException {
        try (StringWriter sw = new StringWriter()) {
            ResultCinemasTheatersSessionsTickets ctst = (ResultCinemasTheatersSessionsTickets) res;
            List<Ticket> tickets = ctst.getTickets();
            HtmlPage page = new HtmlPage("Tickets" ,
                    Html.p(Html.text("Tickets for Session " + tickets.get(0).getSession().getId())),
                    generateTable(tickets));
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
