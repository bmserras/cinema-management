package pt.isel.ls.view.html;

import pt.isel.ls.common.CompositeWritable;
import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Movie;
import pt.isel.ls.entities.Theater;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasID;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import static pt.isel.ls.html.Html.*;

public class ViewHtmlCinemasID extends ViewHtml {

    private String content;

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public ViewHtml html(Result res) throws InternalErrorException {
        try (StringWriter sw = new StringWriter()) {
            Cinema cinema = ((ResultCinemasID)res).getCinema();
            List<Theater> theaters = ((ResultCinemasID)res).getTheaters();
            List<Movie> movies = ((ResultCinemasID)res).getMovies();
            HtmlPage page = new HtmlPage(
                    "CINEMA " + cinema.getId(),
                    img("https://image.flaticon.com/icons/png/128/263/263068.png"),
                    br(),
                    h1(text("Cinema Info")),
                    generateInfo(cinema),
                    br(),
                    style(),
                    theaters.isEmpty()
                            ? h3(text("There are no theaters in this cinema"))
                            : new CompositeWritable(h3(text("Theaters in this cinema")), generateTable(theaters),
                    br(),
                    movies.isEmpty()
                            ? h3(text("There are no movies in this cinema"))
                            : new CompositeWritable(h3(text("Movies in this cinema")), generateTable(movies))),
                    br(),
                    p(a("/cinemas/" + cinema.getId() + "/sessions/date/today", "See all sessions for today in this cinema")),
                    p(a("/cinemas", "Go to all cinemas")),
                    br(),
                    h3(text("Post a Theater")),
                    form("POST","/cinemas/" + cinema.getId() + "/theaters",
                            label("name","Name "), br(), textInput("name"),
                            br(), br(),
                            label("rows","Rows "), br(), textInput("rows"),
                            br(), br(),
                            label("seats","Seats "), br(), textInput("seats"),
                            br(), br(),
                            buttonSubmit("Submit")
                    ),
                    br(),
                    p(aImg("/", "https://i.imgur.com/YLpn7AT.png"))

            );
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
