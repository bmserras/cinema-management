package pt.isel.ls.view.html;

import pt.isel.ls.common.CompositeWritable;
import pt.isel.ls.entities.Session;
import pt.isel.ls.entities.Theater;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasTheatersID;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import static pt.isel.ls.html.Html.*;

public class ViewHtmlCinemasTheatersID extends ViewHtml {

    String content;

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public ViewHtml html(Result res) throws InternalErrorException {
        try (StringWriter sw = new StringWriter()) {
            Theater theater = ((ResultCinemasTheatersID)res).getTheater();
            List<Session> sessions = ((ResultCinemasTheatersID)res).getSessions();
            HtmlPage page = new HtmlPage(
                    "THEATER " + theater.getId(),
                    img("http://www.iconsalot.com/asset/icons/smashicons/movies-3/128/cinema-1-icon.png"),
                    br(),
                    h1(text("Theater Info")),
                    p(generateInfo(theater)),
                    br(),
                    style(),
                    sessions.isEmpty()
                            ? h3(text("There are no sessions in this theater"))
                            : new CompositeWritable(h3(text("Sessions in this theater")), generateSessionsTable(sessions, this)),
                    br(),
                    p(a("/cinemas/" + theater.getCinema().getId(), "Go to the cinema info")),
                    br(),
                    h3(text("Post a session")),
                    form("POST","/cinemas/" + theater.getCinema().getId() + "/theaters/" + theater.getId() + "/sessions",
                            label("date","Date"), br(), textInput("date"),
                            br(), br(),
                            label("mid","Movie ID"), br(), textInput("mid"),
                            br(), br(),
                            buttonSubmit("Submit")
                    ),
                    br(),
                    p(aImg("/", "https://i.imgur.com/YLpn7AT.png"))
            );

            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
