package pt.isel.ls.view.html;

import pt.isel.ls.commands.Listen;
import pt.isel.ls.common.CompositeWritable;
import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.Html;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultMoviesSessionsDate;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

public class ViewHtmlMoviesSessionsDate extends ViewHtml {

    String content;

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public ViewHtml html(Result res) throws InternalErrorException {
        ResultMoviesSessionsDate msd = (ResultMoviesSessionsDate) res;
        List<Session> sessions = msd.getSessions();
        try (StringWriter sw = new StringWriter()) {
            HtmlPage page = new HtmlPage(
                    "SESSIONS",
                    Html.style(),
                    sessions.isEmpty()
                            ? Html.h3(Html.text("There are no sessions"))
                            : new CompositeWritable(Html.h1(Html.text("All sessions from this movie in this day")), generateTable(sessions))
            );
            page.writeTo(sw);
            content = sw.toString();
        } catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
