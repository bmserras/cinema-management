package pt.isel.ls.view.html;

import pt.isel.ls.commands.Listen;
import pt.isel.ls.common.CompositeWritable;
import pt.isel.ls.entities.Cinema;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.Html;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemas;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import static pt.isel.ls.html.Html.*;

public class ViewHtmlError {

    private String content;

    private String msg;
    private String backUrl;

    public ViewHtmlError(String msg, String backUrl) {
        this.msg = msg;
        this.backUrl = backUrl;
    }

    public ViewHtmlError html() throws InternalErrorException {
        try (StringWriter sw = new StringWriter()) {
            HtmlPage page = new HtmlPage(
                    "ERROR",
                    Html.h3(Html.text(msg)),
                    br(),
                    p(aImg(backUrl, "https://www.shareicon.net/data/64x64/2015/12/19/689672_arrows_512x512.png")),
                    br(),
                    br(),
                    p(aImg("/", "https://i.imgur.com/YLpn7AT.png"))
            );
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

    public String getContent() {
        return content;
    }

}
