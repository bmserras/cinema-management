package pt.isel.ls.view.html;

import pt.isel.ls.common.CompositeWritable;
import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasSessions;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import static pt.isel.ls.html.Html.*;

public class ViewHtmlCinemasSessions extends ViewHtml {

    private String content;

    @Override
    public String getContent() {
        return content;
    }

    public ViewHtml html(Result res) throws InternalErrorException {
        try (StringWriter sw = new StringWriter()) {
            List<Session> sessions = ((ResultCinemasSessions)res).getSessions();
            HtmlPage page = new HtmlPage(
                    "SESSIONS" ,
                    style(),
                    sessions.isEmpty()
                            ? h3(text("There are no sessions in this cinema"))
                            : new CompositeWritable(h3(text("Sessions in this cinema")), generateTable(sessions))
            );
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
