package pt.isel.ls.view.html;

import pt.isel.ls.common.CompositeWritable;
import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.Html;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasTheatersSessions;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

public class ViewHtmlCinemasTheatersSessions extends ViewHtml {

    String content;

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public ViewHtml html(Result res) throws InternalErrorException {
        ResultCinemasTheatersSessions cts = (ResultCinemasTheatersSessions) res;
        List<Session> sessions = cts.getSessions();
        try (StringWriter sw = new StringWriter()) {
            HtmlPage page = new HtmlPage(
                    "SESSIONS" ,
                    Html.style(),
                    sessions.isEmpty()
                            ? Html.h3(Html.text("There are no sessions in this theater"))
                            : new CompositeWritable(Html.h3(Html.text("Sessions in this Theater")), generateTable(sessions))
            );
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
