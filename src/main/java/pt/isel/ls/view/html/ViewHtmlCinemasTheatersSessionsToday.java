package pt.isel.ls.view.html;

import pt.isel.ls.common.CompositeWritable;
import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.Html;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasTheatersSessionsToday;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

public class ViewHtmlCinemasTheatersSessionsToday extends ViewHtml {

    String content;

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public ViewHtml html(Result res) throws InternalErrorException {
        try (StringWriter sw = new StringWriter()) {
            ResultCinemasTheatersSessionsToday rctst = (ResultCinemasTheatersSessionsToday) res;
            List<Session> sessions = rctst.getSessions();
            HtmlPage page = new HtmlPage("Sessions",
                    Html.style(),
                    sessions.isEmpty()
                            ? Html.h3(Html.text("There are no sessions today"))
                            : new CompositeWritable(Html.h3(Html.text("Sessions today")), generateTable(sessions)));
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }


}
