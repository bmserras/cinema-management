package pt.isel.ls.view.html;

import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.Html;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasSessionsID;

import java.io.IOException;
import java.io.StringWriter;

public class ViewHtmlCinemasSessionID extends ViewHtml {

    String content;

    @Override
    public String getContent() {
        return content;
    }

    public ViewHtml html(Result res) throws InternalErrorException {
        ResultCinemasSessionsID csi = (ResultCinemasSessionsID) res;
        Session session = csi.getSession();
        try (StringWriter sw = new StringWriter()) {
            HtmlPage page = new HtmlPage("SESSION " + session.getId(),
                    Html.p(Html.text("Session " + session.getId())),
                    Html.p(generateInfo(session)));
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
