package pt.isel.ls.view.html;

import pt.isel.ls.common.Writable;
import pt.isel.ls.entities.*;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.Html;
import pt.isel.ls.html.HtmlElem;
import pt.isel.ls.results.*;
import pt.isel.ls.utils.Pair;
import pt.isel.ls.view.View;

import java.util.HashMap;
import java.util.List;

public abstract class ViewHtml extends View {

    public abstract ViewHtml html(Result res) throws InternalErrorException;

    public static HashMap<String, ViewHtml> htmlMap = new HashMap<>();

    public static void addViews() {

        htmlMap.put(ResultEmpty.class.getName(), new ViewHtmlHome());
        htmlMap.put(ResultOption.class.getName(), new ViewHtmlOption());

        htmlMap.put(ResultCinemas.class.getName(), new ViewHtmlCinemas());
        htmlMap.put(ResultCinemasID.class.getName(), new ViewHtmlCinemasID());

        htmlMap.put(ResultMovies.class.getName(), new ViewHtmlMovies());
        htmlMap.put(ResultMoviesID.class.getName(), new ViewHtmlMoviesID());

        htmlMap.put(ResultCinemasTheaters.class.getName(), new ViewHtmlCinemasTheaters());
        htmlMap.put(ResultCinemasTheatersID.class.getName(), new ViewHtmlCinemasTheatersID());

        htmlMap.put(ResultCinemasTheatersSessions.class.getName(), new ViewHtmlCinemasTheatersSessions());
        htmlMap.put(ResultCinemasTheatersSessionsID.class.getName(), new ViewHtmlCinemasTheatersSessionsID());

        htmlMap.put(ResultCinemasTheatersSessionsTickets.class.getName(), new ViewHtmlCinemasTheatersSessionsTickets());
        htmlMap.put(ResultCinemasTheatersSessionsTicketsID.class.getName(), new ViewHtmlCinemasTheatersSessionsTicketsID());

        htmlMap.put(ResultCinemasSessions.class.getName(), new ViewHtmlCinemasSessions());
        htmlMap.put(ResultCinemasSessionsID.class.getName(), new ViewHtmlCinemasSessionID());

        htmlMap.put(ResultCinemasSessionsDate.class.getName(), new ViewHtmlCinemasSessionsDate());
        htmlMap.put(ResultCinemasSessionsDateToday.class.getName(), new ViewHtmlCinemasSessionsDateToday());
        htmlMap.put(ResultCinemasTheatersSessionsToday.class.getName(), new ViewHtmlCinemasTheatersSessionsToday());
        htmlMap.put(ResultMoviesSessionsDate.class.getName(), new ViewHtmlMoviesSessionsDate());

        htmlMap.put(ResultCinemasTheatersSessionsTicketsAvailable.class.getName(), new ViewHtmlCinemasTheatersSessionsTicketsAvailable());

    }

    public static Writable generateTable(List<? extends Entity> list) {
        HtmlElem table = (HtmlElem)Html.table();
        HtmlElem result = (HtmlElem)Html.tr();
        for (Pair<String, String> pair : list.get(0).toListDate()) {
            result.withContent(Html.th(Html.text(pair.getFirst())));
        }
        table.withContent(result);
        for (Entity e : list) {
            result = (HtmlElem)Html.tr();
            for (Pair<String, String> pair : e.toListDate()) {
                if (pair.getFirst().equals("ID")) {
                    String path = "";
                    if (e instanceof Cinema) {
                        path = "/cinemas/";
                    }
                    else if (e instanceof Movie) {
                        path = "/movies/";
                    }
                    else if (e instanceof Theater) {
                        path = "/cinemas/" + ((Theater) e).getCinema().getId() +
                                "/theaters/";
                    }
                    else if (e instanceof Session) {
                        path = "/cinemas/" + ((Session) e).getTheater().getCinema().getId() +
                                "/theaters/" + ((Session) e).getTheater().getId() +
                                "/sessions/";
                    }
                    else if (e instanceof Ticket) {
                        path = "/cinemas/" + ((Ticket) e).getSession().getTheater().getCinema().getId() +
                                "/theaters/" + ((Ticket) e).getSession().getTheater().getId() +
                                "/sessions/" + ((Ticket) e).getSession().getId() +
                                "/tickets/";
                    }
                    result.withContent(Html.td(Html.a(path + pair.getSecond(), pair.getSecond())));
                }
                else result.withContent(Html.td(Html.text(pair.getSecond())));
            }
            table.withContent(result);
        }
        return table;
    }

    public static Writable generateSessionsTable(List<Session> list, ViewHtml v) {
        HtmlElem table = (HtmlElem)Html.table();
        HtmlElem result = (HtmlElem)Html.tr();
        Session ses = list.get(0);
        List<Pair<String, String>> pairs = getSessionsList(ses, v);
        for (Pair<String, String> pair : pairs) {
            result.withContent(Html.th(Html.text(pair.getFirst())));
        }
        table.withContent(result);
        for (Session s : list) {
            result = (HtmlElem)Html.tr();
            pairs = getSessionsList(s, v);
            for (Pair<String, String> pair : pairs) {
                if (pair.getFirst().equals("ID")) {
                    String path = "/cinemas/" +  s.getTheater().getCinema().getId() +
                            "/theaters/" + s.getTheater().getId() +
                            "/sessions/";
                    result.withContent(Html.td(Html.a(path + pair.getSecond(), pair.getSecond())));
                }
                else result.withContent(Html.td(Html.text(pair.getSecond())));
            }
            table.withContent(result);
        }
        return table;
    }

    private static List<Pair<String, String>> getSessionsList(Session s, ViewHtml v) {
        List<Pair<String, String>> pairs;
        if (v instanceof ViewHtmlCinemasSessionsDateToday || v instanceof ViewHtmlCinemasSessionsDate) pairs = s.toListTheater();
        else if (v instanceof ViewHtmlCinemasTheatersID) pairs = s.toListMovie();
        else if (v instanceof ViewHtmlMoviesID) pairs = s.toListDate();
        else pairs = s.toListDetailed();
        return pairs;
    }

    public static Writable generateInfo(Entity entity) {
        List<Pair<String, String>> list = entity.toListDetailed();
        HtmlElem uls = (HtmlElem)Html.ul();
        for (Pair<String, String> pair : list) {
            uls.withContent(Html.li(Html.text(pair.getFirst() + " : " + pair.getSecond())));
        }
        return uls;
    }

    public static Writable generateInfo(List<Pair<String, String>> list) {
        HtmlElem uls = (HtmlElem)Html.ul();
        for (Pair<String, String> pair : list) {
            uls.withContent(Html.li(Html.text(pair.getFirst() + " : " + pair.getSecond())));
        }
        return uls;
    }

}
