package pt.isel.ls.view.html;

import pt.isel.ls.common.CompositeWritable;
import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasSessionsDateToday;

import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.List;

import static pt.isel.ls.html.Html.*;

public class ViewHtmlCinemasSessionsDateToday extends ViewHtml {

    private String content;

    @Override
    public String getContent() {
        return content;
    }

    public ViewHtml html(Result res) throws InternalErrorException {
        try (StringWriter sw = new StringWriter()) {
            List<Session> sessions = ((ResultCinemasSessionsDateToday)res).getSessions();
            Cinema cinema = ((ResultCinemasSessionsDateToday) res).getCinema();
            LocalDateTime localDateTime = ((ResultCinemasSessionsDateToday) res).getLocalDateTime();
            HtmlPage page = new HtmlPage("SESSIONS",
                    img("https://www.shareicon.net/data/128x128/2016/09/01/822771_cinema_512x512.png"),
                    br(), br(),
                    style(),
                    sessions.isEmpty()
                            ? h3(text("There are no sessions today in " + cinema.getName()))
                            : new CompositeWritable(h3(text("Sessions for today in " + cinema.getName())), generateSessionsTable(sessions, this)),
                    br(),
                    p(aImg("/cinemas/" + ((ResultCinemasSessionsDateToday) res).getCinema().getId() +
                            "/sessions/date/" + localDateTime.plusDays(1).toLocalDate().toString(), "https://cdn1.iconfinder.com/data/icons/symbol-black-common-4/32/calendar-next-64.png")),
                    p(aImg("/cinemas/" + ((ResultCinemasSessionsDateToday) res).getCinema().getId() +
                            "/sessions/date/" + localDateTime.minusDays(1).toLocalDate().toString(), "https://cdn1.iconfinder.com/data/icons/symbol-black-common-4/32/calendar-previous-64.png")),
                    br(),
                    p(a("/cinemas/" + ((ResultCinemasSessionsDateToday) res).getCinema().getId(), "Go to the cinema info")),
                    br(), br(),
                    p(aImg("/", "https://i.imgur.com/YLpn7AT.png"))
            );
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
