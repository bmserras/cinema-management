package pt.isel.ls.view.html;

import pt.isel.ls.entities.Ticket;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasTheatersSessionsTicketsID;

import java.io.IOException;
import java.io.StringWriter;

import static pt.isel.ls.html.Html.*;

public class ViewHtmlCinemasTheatersSessionsTicketsID extends ViewHtml {

    private String content;

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public ViewHtml html(Result res) throws InternalErrorException {
        try (StringWriter sw = new StringWriter()) {
            ResultCinemasTheatersSessionsTicketsID rctst = (ResultCinemasTheatersSessionsTicketsID) res;
            Ticket ticket = rctst.getTicket();
            HtmlPage page = new HtmlPage("TICKET " + ticket.getId(),
                    img("https://image.flaticon.com/icons/png/128/3/3655.png"),
                    br(),
                    h1(text("Ticket Info")),
                    p(generateInfo(ticket)),
                    br(),
                    p(a("/cinemas/" + rctst.getSession().getTheater().getCinema().getId() +
                            "/theaters/" + rctst.getSession().getTheater().getId() + "/sessions/" + rctst.getSession().getId(),"Go to the session info")),
                    br(),
                    p(aImg("/", "https://i.imgur.com/YLpn7AT.png"))
            );
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
