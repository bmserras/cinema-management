package pt.isel.ls.view.html;

import pt.isel.ls.common.CompositeWritable;
import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Movie;
import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultMoviesID;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import static pt.isel.ls.html.Html.*;

public class ViewHtmlMoviesID extends ViewHtml {

    private String content;

    @Override
    public String getContent() {
        return content;
    }

    public ViewHtml html(Result res) throws InternalErrorException {
        try (StringWriter sw = new StringWriter()) {
            Movie movie = ((ResultMoviesID)res).getMovie();
            List<Cinema> cinemas = ((ResultMoviesID)res).getCinemas();
            List<Session> sessions = ((ResultMoviesID)res).getSessions();
            HtmlPage page = new HtmlPage(
                    "MOVIE " + movie.getId(),
                    img("https://www.shareicon.net/data/128x128/2016/05/22/769208_cinema_512x512.png"),
                    br(),
                    h1(text("Movie Info")),
                    p(generateInfo(movie)),
                    br(),
                    style(),
                    cinemas.isEmpty()
                            ? h3(text("There are no cinemas displaying this movie"))
                            : new CompositeWritable(h3(text("Cinemas with this movie available")), generateTable(cinemas)),
                    br(),
                    sessions.isEmpty()
                            ? h3(text("There are no sessions for this movie"))
                            : new CompositeWritable(h3(text("Sessions with this movie")), generateSessionsTable(sessions, this)),
                    br(),
                    p(a("/movies", "Go to all movies")),
                    br(),
                    p(aImg("/", "https://i.imgur.com/YLpn7AT.png"))
            );
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
