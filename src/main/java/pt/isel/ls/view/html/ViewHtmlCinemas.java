package pt.isel.ls.view.html;

import pt.isel.ls.common.CompositeWritable;
import pt.isel.ls.entities.Cinema;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemas;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import static pt.isel.ls.html.Html.*;

public class ViewHtmlCinemas extends ViewHtml {

    private String content;

    @Override
    public String getContent() {
        return content;
    }
    
    public ViewHtml html(Result res) throws InternalErrorException {
        try (StringWriter sw = new StringWriter()) {
            List<Cinema> cinemas = ((ResultCinemas)res).getCinemas();
            HtmlPage page = new HtmlPage(
                    "CINEMAS",
                    img("https://image.flaticon.com/icons/png/128/263/263068.png"),
                    br(),
                    style(),
                    cinemas.isEmpty()
                            ? h3(text("There are no cinemas"))
                            : new CompositeWritable(h1(text("All cinemas")), generateTable(cinemas)),
                    br(),
                    h3(text("Post a cinema")),
                    form("POST", "/cinemas",
                            label("name", "Name"), br(), textInput("name"),
                            br(), br(),
                            label("city", "City"), br(), textInput("city"),
                            br(), br(),
                            buttonSubmit("Submit")
                    ),
                    br(),
                    p(aImg("/", "https://i.imgur.com/YLpn7AT.png"))
            );
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
