package pt.isel.ls.view.html;

import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.Html;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasTheatersSessionsTicketsAvailable;

import java.io.IOException;
import java.io.StringWriter;

public class ViewHtmlCinemasTheatersSessionsTicketsAvailable extends ViewHtml {

    String content;

    @Override
    public String getContent() {return content;}

    @Override
    public ViewHtml html(Result res) throws InternalErrorException {
        ResultCinemasTheatersSessionsTicketsAvailable rct = (ResultCinemasTheatersSessionsTicketsAvailable)res;
        Session session = rct.getSession();
        try (StringWriter sw = new StringWriter()) {
            HtmlPage page = new HtmlPage(
                    "SESSION " + session.getId(),
                    Html.h1(Html.text("Session " + session.getId())),
                    Html.p(generateInfo(session)),
                    Html.h3(Html.text("There are " + (session.getTheater().getSeats() - rct.getTicketsAvailable()) + " tickets available in this session")));
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
