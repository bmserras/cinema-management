package pt.isel.ls.view.html;

import pt.isel.ls.common.CompositeWritable;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;

import java.io.IOException;
import java.io.StringWriter;

import static pt.isel.ls.html.Html.*;

public class ViewHtmlHome extends ViewHtml {

    private String content;

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public ViewHtml html(Result res) throws InternalErrorException{
        try (StringWriter sw = new StringWriter()) {
            HtmlPage page = new HtmlPage(
                    "HOME",
                    h1(text("Welcome to the Cinema Management App!")),
                    br(),
                    h2(a("/cinemas", "All cinemas")),
                    img("https://image.flaticon.com/icons/png/128/263/263068.png"),
                    br(), br(), br(),
                    h2(a("/movies", "All movies")),
                    img("https://www.shareicon.net/data/128x128/2016/05/22/769208_cinema_512x512.png"),
                    br(), br(), br(), br(), br(),
                    hr(),
                    new CompositeWritable(
                            small(text("Developed by Group 8 (Bruno Serras, Filipe Jesus, Marcos Miranda)")), br(),
                            small(text("Laboratório de Software")), br(),
                            small(text("ISEL - 2018"))
                    )
            );
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
