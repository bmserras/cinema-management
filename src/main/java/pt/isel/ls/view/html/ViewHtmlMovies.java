package pt.isel.ls.view.html;

import pt.isel.ls.common.CompositeWritable;
import pt.isel.ls.entities.Movie;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultMovies;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import static pt.isel.ls.html.Html.*;

public class ViewHtmlMovies extends ViewHtml {

    private String content;

    @Override
    public String getContent() {
        return content;
    }

    public ViewHtml html(Result res) throws InternalErrorException {
        try (StringWriter sw = new StringWriter()) {
            List<Movie> movies = ((ResultMovies)res).getMovies();
            HtmlPage page = new HtmlPage(
                    "MOVIES",
                    img("https://www.shareicon.net/data/128x128/2016/05/22/769208_cinema_512x512.png"),
                    br(),
                    style(),
                    movies.isEmpty()
                            ? h3(text("There are no movies"))
                            : new CompositeWritable(h1(text("All movies")), generateTable(movies)),
                    br(),
                    h3(text("Post a movie")),
                    form("POST","/movies",
                            label("id","Movie ID "), br(), textInput("id"),
                            br(), br(),
                            buttonSubmit("Submit")
                    ),
                    br(),
                    p(aImg("/", "https://i.imgur.com/YLpn7AT.png"))
            );
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
