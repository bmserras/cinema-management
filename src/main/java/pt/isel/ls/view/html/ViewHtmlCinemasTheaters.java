package pt.isel.ls.view.html;

import pt.isel.ls.common.CompositeWritable;
import pt.isel.ls.entities.Theater;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.Html;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasTheaters;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

public class ViewHtmlCinemasTheaters extends ViewHtml {

    private String content;

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public ViewHtml html(Result res) throws InternalErrorException {
        try (StringWriter sw = new StringWriter()) {
            List<Theater> theaters = ((ResultCinemasTheaters)res).getTheaters();
            HtmlPage page = new HtmlPage(
                    "Theaters",
                    Html.style(),
                    theaters.isEmpty()
                            ? Html.h3(Html.text("There are no theaters in this cinema"))
                            : new CompositeWritable(Html.p(Html.text("Theaters in this cinema")), generateTable(theaters))
            );
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
