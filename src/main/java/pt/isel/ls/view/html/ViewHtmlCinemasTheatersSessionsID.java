package pt.isel.ls.view.html;

import pt.isel.ls.common.CompositeWritable;
import pt.isel.ls.entities.Session;
import pt.isel.ls.entities.Ticket;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.Html;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasTheatersSessionsID;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import static pt.isel.ls.html.Html.*;

public class ViewHtmlCinemasTheatersSessionsID extends ViewHtml {

    private String content;

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public ViewHtml html(Result res) throws InternalErrorException {
        try (StringWriter sw = new StringWriter()) {
            Session session = ((ResultCinemasTheatersSessionsID)res).getSession();
            List<Ticket> tickets = ((ResultCinemasTheatersSessionsID)res).getTickets();
            int ticketsAvailable = session.getTheater().getSeats() - tickets.size();
            HtmlPage page = new HtmlPage(
                    "SESSION " + session.getId(),
                    img("https://www.shareicon.net/data/128x128/2016/09/01/822771_cinema_512x512.png"),
                    br(),
                    h1(text("Session Info")),
                    p(generateInfo(session)),
                    br(),
                    ticketsAvailable == 0
                            ? h2(text("This session is SOLD OUT"))
                            : h2(text("There are " + ticketsAvailable + " tickets available in this session")),
                    br(),
                    style(),
                    tickets.isEmpty()
                            ? h3(text("No one bought tickets yet for this session"))
                            : new CompositeWritable(h3(text("Tickets in this session")), generateTable(tickets)),
                    br(),
                    p(a("/cinemas/" + session.getTheater().getCinema().getId() +
                            "/sessions/date/today", "See all sessions for today in this cinema")),
                    p(a("/cinemas/" + session.getTheater().getCinema().getId() +
                            "/theaters/" + session.getTheater().getId(), "Go to the theater info")),
                    br(),
                    h3(text("Post a ticket")),
                    form("POST","/cinemas/" + session.getTheater().getCinema().getId() + "/theaters/" + session.getTheater().getId() + "/sessions/" + session.getId() + "/tickets",
                            label("row","Row"), br(), textInput("row"),
                            br(), br(),
                            label("seat","Seat"), br(), textInput("seat"),
                            br(), br(),
                            buttonSubmit("Submit")
                    ),
                    br(),
                    p(aImg("/", "https://i.imgur.com/YLpn7AT.png"))

            );
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
