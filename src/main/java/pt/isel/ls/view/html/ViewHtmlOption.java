package pt.isel.ls.view.html;

import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.html.Html;
import pt.isel.ls.html.HtmlPage;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultOption;
import pt.isel.ls.utils.Pair;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

public class ViewHtmlOption extends ViewHtml {

    private String content;

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public ViewHtml html(Result res) throws InternalErrorException {
        try (StringWriter sw = new StringWriter()) {
            List<Pair<String, String>> options = ((ResultOption)res).getOptions();
            HtmlPage page = new HtmlPage(
                    "OPTIONS",
                    Html.h1(Html.text("All options")),
                    generateInfo(options)
            );
            page.writeTo(sw);
            content = sw.toString();
        }
        catch (IOException e) {
            throw new InternalErrorException(e.getMessage(), e);
        }
        return this;
    }

}
