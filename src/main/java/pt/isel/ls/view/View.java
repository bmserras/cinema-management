package pt.isel.ls.view;

import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.results.Result;
import pt.isel.ls.headers.Header;
import pt.isel.ls.view.html.ViewHtml;
import pt.isel.ls.view.json.ViewJson;
import pt.isel.ls.view.plain.ViewPlain;

import java.io.IOException;
import java.io.PrintWriter;

public abstract class View {

    public abstract String getContent();

    public static void addViews() {
        ViewHtml.addViews();
        ViewPlain.addViews();
        ViewJson.addViews();
    }

    public static View getView(Result rs, Header header) throws InternalErrorException {
        View v = null;
        switch (header.getName()) {
            case "text/html":
                v = ViewHtml.htmlMap.get(rs.getClass().getName()); ((ViewHtml)v).html(rs); return v;
            case "text/plain":
                v = ViewPlain.plainMap.get(rs.getClass().getName()); ((ViewPlain)v).plain(rs); return v;
            case "application/json":
                v =  ViewJson.jsonMap.get(rs.getClass().getName()); ((ViewJson)v).json(rs); return v;
            default:
                return v;
        }
    }

    public void output(String content, Header header) throws InternalErrorException {
        if (header.isFile()) {
            try (PrintWriter writer = new PrintWriter(header.getFileName())) {
                writer.println(content);
            }
            catch (IOException e) {
                throw new InternalErrorException(e.getMessage(), e);
            }
        }
        else {
            System.out.println(content);
        }
    }

}
