package pt.isel.ls.view.plain;

import pt.isel.ls.entities.Entity;
import pt.isel.ls.results.Result;
import pt.isel.ls.utils.Pair;

public class ViewPlainNotDetailed extends ViewPlain {

    private String content;
    private String title;

    public ViewPlainNotDetailed(String title) {
        this.title = title;
    }

    @Override
    public ViewPlain plain(Result res) {
        StringBuilder sb = new StringBuilder();
        sb.append(title);
        sb.append("\r\n");
        sb.append("\r\n");
        for (Entity e : res.getList()) {
            for (Pair<String, String> pair : e.toListDate()) {
                sb.append(pair.getFirst());
                sb.append(": ");
                sb.append(pair.getSecond());
                sb.append("\r\n");
            }
            sb.append("\r\n");
        }
        content = sb.toString();
        return this;
    }

    @Override
    public String getContent() {
        return content;
    }
}
