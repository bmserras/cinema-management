package pt.isel.ls.view.plain;

import pt.isel.ls.entities.Entity;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultOption;
import pt.isel.ls.results.ResultPost;
import pt.isel.ls.utils.Pair;

import java.util.List;

public class ViewPlainDetailed extends ViewPlain {

    private String content;
    private String title;

    public ViewPlainDetailed(String title) {
        this.title = title;
    }

    @Override
    public ViewPlain plain(Result res) {
        StringBuilder sb = new StringBuilder();
        sb.append(title);
        sb.append("\r\n");
        sb.append("\r\n");
        List<Pair<String ,String>> list;
        if(res instanceof ResultOption){
            list = ((ResultOption) res).getOptions();
        }else{
            list = res.getDetailedList();
        }
        for (Pair<String, String> pair : list) {
            sb.append(pair.getFirst());
            sb.append(": ");
            sb.append(pair.getSecond());
            sb.append("\r\n");
        }


        sb.append("\r\n");
        content = sb.toString();
        return this;
    }

    @Override
    public String getContent() {
        return content;
    }
}
