package pt.isel.ls.view.plain;

import pt.isel.ls.results.*;
import pt.isel.ls.view.View;

import java.util.HashMap;

public abstract class ViewPlain extends View {

    public abstract ViewPlain plain(Result res);

    public static HashMap<String, ViewPlain> plainMap = new HashMap<>();

    public static void addViews() {

        plainMap.put(ResultCinemas.class.getName(), new ViewPlainNotDetailed("CINEMAS"));
        plainMap.put(ResultCinemasID.class.getName(), new ViewPlainDetailed("CINEMA"));

        plainMap.put(ResultMovies.class.getName(), new ViewPlainNotDetailed("MOVIES"));
        plainMap.put(ResultMoviesID.class.getName(), new ViewPlainDetailed("MOVIE"));

        plainMap.put(ResultCinemasTheaters.class.getName(), new ViewPlainNotDetailed("THEATERS"));
        plainMap.put(ResultCinemasTheatersID.class.getName(), new ViewPlainDetailed("THEATER"));

        plainMap.put(ResultCinemasTheatersSessions.class.getName(), new ViewPlainNotDetailed("SESSIONS"));
        plainMap.put(ResultCinemasTheatersSessionsID.class.getName(), new ViewPlainDetailed("SESSION"));

        plainMap.put(ResultCinemasTheatersSessionsTickets.class.getName(), new ViewPlainNotDetailed("TICKETS"));
        plainMap.put(ResultCinemasTheatersSessionsTicketsID.class.getName(), new ViewPlainDetailed("TICKET"));

        plainMap.put(ResultCinemasSessions.class.getName(), new ViewPlainNotDetailed("SESSIONS"));
        plainMap.put(ResultCinemasSessionsID.class.getName(), new ViewPlainDetailed("SESSION"));

        plainMap.put(ResultCinemasSessionsDate.class.getName(), new ViewPlainNotDetailed("SESSIONS"));
        plainMap.put(ResultCinemasSessionsDateToday.class.getName(), new ViewPlainNotDetailed("SESSIONS"));
        plainMap.put(ResultCinemasTheatersSessionsToday.class.getName(), new ViewPlainNotDetailed("SESSIONS"));
        plainMap.put(ResultMoviesSessionsDate.class.getName(), new ViewPlainNotDetailed("SESSIONS"));

        plainMap.put(ResultCinemasTheatersSessionsTicketsAvailable.class.getName(), new ViewPlainDetailed("SESSION"));

        plainMap.put(ResultOption.class.getName(),new ViewPlainDetailed("OPTIONS"));

    }

}
