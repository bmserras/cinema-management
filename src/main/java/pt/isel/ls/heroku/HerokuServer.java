package pt.isel.ls.heroku;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.isel.ls.commands.Command;
import pt.isel.ls.utils.Pair;

import java.util.HashMap;

import static pt.isel.ls.commands.CommandManager.addCommands;
import static pt.isel.ls.commands.CommandManager.getCommandAndValues;
import static pt.isel.ls.headers.HeaderManager.addHeaders;
import static pt.isel.ls.view.View.addViews;

public class HerokuServer {

    /*
     * TCP port where to listen.
     * Standard port for HTTP is 80 but might be already in use
     */
    private static final int LISTEN_PORT = 5432;

    public static void main(String[] args) throws Exception {

        System.setProperty("org.slf4j.simpleLogger.levelInBrackets", "true");

        Logger logger = LoggerFactory.getLogger(HerokuServer.class);
        logger.info("Starting main...");

        addCommands();
        addHeaders();
        addViews();

        Pair<Command, HashMap<String, String>> pair = getCommandAndValues("LISTEN", "/");

        pair.getFirst().execute(null, "port=" + LISTEN_PORT);

    }
}