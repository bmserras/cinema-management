package pt.isel.ls.db;

import pt.isel.ls.webapi.MovieWebApi;
import pt.isel.ls.entities.Movie;
import pt.isel.ls.exceptions.DBException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.*;

public class MovieDB extends EntityDB {

    private final MovieWebApi webApi;

    public MovieDB(MovieWebApi WebApi) {
        this.webApi = WebApi;
    }

    public MovieWebApi getWebApi() {
        return webApi;
    }

    // returns a list with all movies
    public static List<Movie> selectMovies(Connection connection) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM MOVIE");
            rs = stmt.executeQuery();
            List<Movie> list = new ArrayList<>();
            while (rs.next()) {
                Movie m = new Movie(rs.getInt("ID"), rs.getString("Title"),
                        rs.getInt("ReleaseYear"), rs.getInt("Duration"));
                list.add(m);
            }
            return list;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // returns a movie identified by mid
    public static Movie selectMovie(Connection connection, int mid) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM MOVIE WHERE ID = ?");
            stmt.setInt(1, mid);
            rs = stmt.executeQuery();
            rs.next();
            Movie m = new Movie(mid, rs.getString("Title"), rs.getInt("ReleaseYear"), rs.getInt("Duration"));
            return m;
        }
        catch (SQLException e) {
            throw new DBException("There is no such movie", e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // return a list of movies that are being displayed in the cinema identified by cid
    public static List<Movie> selectCinemasMovies(Connection connection, int cid) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT DISTINCT m.ID, m.Title, m.ReleaseYear, m.Duration " +
                    "FROM MOVIE AS m, Session AS s WHERE s.CinemaID = ? AND s.MovieID = m.ID");
            stmt.setInt(1, cid);
            rs = stmt.executeQuery();
            List<Movie> list = new ArrayList<>();
            while (rs.next()) {
                Movie m = new Movie(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4));
                list.add(m);
            }
            return list;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // inserts a movie m
    public static int insertMovie(Connection connection, Movie m) throws DBException {
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement("INSERT INTO MOVIE VALUES(?,?,?,?)");
            stmt.setInt(1, m.getId());
            stmt.setString(2, m.getTitle());
            stmt.setInt(3, parseInt(m.getRelease_date().split("-")[0]));
            stmt.setInt(4, m.getRuntime());
            stmt.executeUpdate();
            return m.getId();
        }
        catch (SQLException e) {
            throw new DBException("That movie already exists", e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

}