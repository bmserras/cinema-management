package pt.isel.ls.db;

import pt.isel.ls.entities.Cinema;
import pt.isel.ls.exceptions.DBException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CinemaDB extends EntityDB {

    // returns a list with all cinemas
    public static List<Cinema> selectCinemas(Connection connection) throws DBException  {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM CINEMA");
            rs = stmt.executeQuery();
            List<Cinema> list = new ArrayList<>();
            while (rs.next()) {
                Cinema c = new Cinema(rs.getInt("ID"), rs.getString("Name"),
                        rs.getString("City"));
                list.add(c);
            }
            return list;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // returns a cinema identified by cid
    public static Cinema selectCinema(Connection connection, int cid) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM CINEMA WHERE ID = ?");
            stmt.setInt(1, cid);
            rs = stmt.executeQuery();
            rs.next();
            Cinema c = new Cinema(cid, rs.getString("Name"), rs.getString("City"));
            return c;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // returns a list of cinemas that are displaying the movie identified by mid
    public static List<Cinema> selectMoviesCinemas(Connection connection, int mid) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT DISTINCT c.ID, c.Name, c.City FROM CINEMA as c,SESSION as s,MOVIE as m WHERE " +
                    "s.MovieID = ? AND m.ID = s.MovieID AND c.ID = s.CinemaID");
            stmt.setInt(1,mid);
            rs = stmt.executeQuery();
            List<Cinema> list = new ArrayList<>();
            while (rs.next()) {
                Cinema c = new Cinema(rs.getInt(1), rs.getString(2), rs.getString(3));
                list.add(c);
            }
            return list;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // inserts a cinema c
    public static int insertCinema(Connection connection, Cinema c) throws DBException {
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement("INSERT INTO CINEMA (Name, City) VALUES(?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, c.getName());
            stmt.setString(2, c.getCity());
            stmt.executeUpdate();
            return getGeneratedID(stmt);
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

}