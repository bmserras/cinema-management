package pt.isel.ls.db;

import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Theater;
import pt.isel.ls.exceptions.DBException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TheaterDB extends EntityDB {

    // returns a list with all theaters from a cinema identified by cid
    public static List<Theater> selectTheaters(Connection connection, int cid) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM THEATER WHERE CinemaID = ?");
            stmt.setInt(1, cid);
            rs = stmt.executeQuery();
            List<Theater> list = new ArrayList<>();
            Cinema c = CinemaDB.selectCinema(connection, cid);
            while (rs.next()) {
                Theater t = new Theater(rs.getInt("ID"), rs.getString("Name"),
                        rs.getInt("NumberOfAvailableSeats"), rs.getInt("NumberOfRows"),
                        rs.getInt("NumberOfSeatsPerRow"), c);
                list.add(t);
            }
            return list;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // returns a theater identified by tid from a cinema identified by cid
    public static Theater selectTheater(Connection connection, int cid, int tid) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM THEATER WHERE ID = ? AND CinemaID = ?");
            stmt.setInt(1, tid);
            stmt.setInt(2, cid);
            rs = stmt.executeQuery();
            rs.next();
            Cinema c = CinemaDB.selectCinema(connection, cid);
            Theater t = new Theater(tid, rs.getString("Name"),
                    rs.getInt("NumberOfAvailableSeats"), rs.getInt("NumberOfRows"),
                    rs.getInt("NumberOfSeatsPerRow"), c);
            return t;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // inserts a theater t
    public static int insertTheater(Connection connection, Theater t) throws DBException {
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement("INSERT INTO THEATER (Name, NumberOfAvailableSeats, NumberOfRows, NumberOfSeatsPerRow, CinemaID) VALUES(?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, t.getName());
            stmt.setInt(2, t.getSeats());
            stmt.setInt(3, t.getRows());
            stmt.setInt(4, t.getSeatsPerRow());
            stmt.setInt(5, t.getCinema().getId());
            stmt.executeUpdate();
            return getGeneratedID(stmt);
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

}