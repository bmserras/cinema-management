package pt.isel.ls.db;

import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.isel.ls.exceptions.DBException;

import java.sql.Connection;
import java.sql.SQLException;

public class Connect {

    private static PGSimpleDataSource ds = new PGSimpleDataSource();
    private String jdbcUrl = System.getenv("JDBC_DATABASE_URL");

    private static final Logger _logger = LoggerFactory.getLogger(Connect.class);

    private static String databaseName = "LSDB"; // by default, LSDB is used

    private Connection connection;

    public Connect() throws DBException {
        this.connection = openConnection();
    }

    public Connection openConnection() throws DBException {

        try {
            ds.setUrl(jdbcUrl);
            connection = ds.getConnection();
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return connection;
    }

    public Connection getConnection() {
        return connection;
    }

    public void closeConnection() throws DBException {
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    public static void setDatabaseName(String databaseName) {
        Connect.databaseName = databaseName;
    }
}
