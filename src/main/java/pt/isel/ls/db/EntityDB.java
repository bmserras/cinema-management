package pt.isel.ls.db;

import pt.isel.ls.exceptions.WrongParametersException;
import pt.isel.ls.results.Result;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class EntityDB {

    public static HashMap<String, String> separateParameters(ArrayList<String> names, String parameters) throws WrongParametersException {
        HashMap<String, String> paramsMap = new HashMap<>();
        String[] params = parameters.split("&");
        int count = 0;
        for (String param : params) {
            String[] split = param.split("=");
            if (split.length != 2) throw new WrongParametersException("Parameter syntax incorrect");
            if (names.contains(split[0])) {
                ++count;
                paramsMap.put(split[0], split[1]);
            }
            else throw new WrongParametersException("Parameter syntax incorrect");
        }
        if (count != names.size()) throw new WrongParametersException("Parameter syntax incorrect");
        return paramsMap;
    }

    public static String[] separateParameters(String[] names, String parameters) throws WrongParametersException {
        String[] params = parameters.split("&");
        for (int i = 0; i < params.length; i++) {
            if (!names[0].equals(params[i].split("=")[0])) {
                throw new WrongParametersException("Parameter syntax incorrect");
            }
            params[i] = params[i].split("=")[1].replace("+", " ");
        }
        return params;
    }

    public static String[] checkOptionalParameter(String[] names, String parameters) throws WrongParametersException {
        String[] param = parameters.split("=");
        if (param[0].isEmpty() | param.length != 2) return param;
        for (String name : names) {
            if (param[0].equals(name)) {
                return param;
            }
        }
        throw new WrongParametersException("Parameter syntax incorrect");
    }

    public static int getGeneratedID(PreparedStatement stmt) throws SQLException {
        ResultSet rs = stmt.getGeneratedKeys();
        rs.next();
        int id = rs.getInt(1);
        rs.close();
        return id;
    }

}
