package pt.isel.ls.db;

import pt.isel.ls.entities.Session;
import pt.isel.ls.entities.Ticket;
import pt.isel.ls.exceptions.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TicketDB extends EntityDB {

    // returns a list of tickets from a cinema identified by cid, a theater identified by tid and a session identifid by sid
    public static List<Ticket> selectTickets(Connection connection, int cid, int tid, int sid) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM TICKET AS tk, CINEMA AS c, THEATER AS t, SESSION AS s, MOVIE AS m WHERE " +
                    "c.ID = ? AND t.ID = ? AND s.ID = ? AND tk.CinemaID = c.ID AND tk.TheaterID = t.ID AND tk.SessionID = s.ID  AND m.ID = s.MovieID");
            stmt.setInt(1, cid);
            stmt.setInt(2, tid);
            stmt.setInt(3, sid);
            rs = stmt.executeQuery();
            List<Ticket> list = new ArrayList<>();
            Session s = SessionDB.selectSession(connection, cid, sid);
            while (rs.next()) {
                Ticket tk = new Ticket(rs.getInt("ID"), rs.getString("Row"), rs.getInt("Seat"), s);
                list.add(tk);
            }
            return list;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // returns a ticket identified by tkid from a cinema identified by cid, a theater identified by tid and a session identifid by sid
    public static Ticket selectTicket(Connection connection, int cid, int tid, int sid, int tkid) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM TICKET AS tk, CINEMA AS c, THEATER AS t, SESSION AS s, MOVIE AS m WHERE " +
                    "c.ID = ? AND t.ID = ? AND s.ID = ? AND tk.CinemaID = c.ID AND tk.TheaterID = t.ID AND tk.SessionID = s.ID AND tk.ID = ? " +
                    "AND m.ID = s.MovieID");
            stmt.setInt(1, cid);
            stmt.setInt(2, tid);
            stmt.setInt(3, sid);
            stmt.setInt(4, tkid);
            rs = stmt.executeQuery();
            rs.next();
            Session s = SessionDB.selectSession(connection, cid, sid);
            Ticket tk = new Ticket(rs.getInt("ID"), rs.getString("Row"), rs.getInt("Seat"), s);
            return tk;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // inserts a ticket tk
    public static int insertTicket(Connection connection, Ticket tk) throws DBException {
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement("INSERT INTO TICKET (Row, Seat, CinemaID, TheaterID, SessionID, MovieID) VALUES(?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, tk.getRow());
            stmt.setInt(2, tk.getSeat());
            stmt.setInt(3, tk.getSession().getTheater().getCinema().getId());
            stmt.setInt(4, tk.getSession().getTheater().getId());
            stmt.setInt(5, tk.getSession().getId());
            stmt.setInt(6, tk.getSession().getMovie().getId());
            stmt.executeUpdate();
            return getGeneratedID(stmt);
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // deletes a ticket from a session s in given the parameters row and seat
    public static int deleteTicket(Connection connection, Session s, String row, int seat) throws DBException {
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement("DELETE FROM TICKET WHERE CinemaID = ? AND TheaterID = ? AND SessionID = ? AND Row = ? AND Seat = ?");
            stmt.setInt(1, s.getTheater().getCinema().getId());
            stmt.setInt(2, s.getTheater().getId());
            stmt.setInt(3, s.getId());
            stmt.setString(4, row);
            stmt.setInt(5, seat);
            return stmt.executeUpdate();
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // return a ticket from a cinema identified by cid, a theater identified by tid and a session identified by sid given the parameters row and seat
    public static Ticket selectTicketByRowSeat(Connection connection, int cid, int tid, int sid, String row, int seat) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM TICKET AS t,SESSION AS s,THEATER AS th,CINEMA AS c WHERE " +
                    "c.ID = ? AND th.ID = ? AND s.ID = ? AND t.Row = ? AND t.Seat = ? AND t.CinemaID = c.ID AND t.TheaterID = th.ID AND t.SessionID = s.ID");
            stmt.setInt(1, cid);
            stmt.setInt(2, tid);
            stmt.setInt(3, sid);
            stmt.setString(4, row);
            stmt.setInt(5, seat);
            rs = stmt.executeQuery();
            if (rs.next()) {
                Session s = SessionDB.selectSession(connection, cid, sid);
                Ticket tk = new Ticket(rs.getInt("ID"), rs.getString("Row"), rs.getInt("Seat"), s);
                return tk;
            }
            return null;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // returns the number of tickets from a session identified by sid
    public static int getNumberOfTickets(Connection connection, int sid) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT COUNT(ID) FROM TICKET WHERE SessionID = ?");
            stmt.setInt(1, sid);
            rs = stmt.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

}