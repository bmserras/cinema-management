package pt.isel.ls.db;

import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Movie;
import pt.isel.ls.entities.Session;
import pt.isel.ls.entities.Theater;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.exceptions.SessionOverlapException;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.*;
import static java.time.LocalDateTime.*;
import static pt.isel.ls.utils.DateUtil.toLocalDateTime;

public class SessionDB extends EntityDB {

    // returns a list with all sessions from a cinema identified by cid
    public static List<Session> selectSessions(Connection connection, int cid) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM SESSION AS s, CINEMA AS c, THEATER t, MOVIE as m " +
                    "WHERE s.CinemaID = ? AND c.ID = s.CinemaID AND s.TheaterID = t.ID AND s.MovieID = m.ID");
            stmt.setInt(1, cid);
            rs = stmt.executeQuery();
            List<Session> list = new ArrayList<>();
            Cinema c = CinemaDB.selectCinema(connection, cid);
            while (rs.next()) {
                Theater t = TheaterDB.selectTheater(connection, c.getId(), rs.getInt("TheaterID"));
                Movie m = MovieDB.selectMovie(connection, rs.getInt("MovieID"));
                Session s = new Session(rs.getInt("ID"), toLocalDateTime(rs.getString("DateTime")), t, m);
                list.add(s);
            }
            return list;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // returns a list with all sessions from a theater identified by tid and a cinema identified by cid
    public static List<Session> selectSessions(Connection connection, int cid, int tid) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM SESSION AS s, CINEMA AS c, THEATER t, MOVIE as m " +
                    "WHERE s.TheaterID = ? AND s.CinemaID = ? AND c.ID = s.CinemaID AND t.ID = s.TheaterID AND s.MovieID = m.ID");
            stmt.setInt(1, tid);
            stmt.setInt(2, cid);
            rs = stmt.executeQuery();
            List<Session> list = new ArrayList<>();
            Theater t = TheaterDB.selectTheater(connection, cid, tid);
            while (rs.next()) {
                Movie m = MovieDB.selectMovie(connection, rs.getInt("MovieID"));
                Session s = new Session(rs.getInt("ID"), toLocalDateTime(rs.getString("DateTime")), t, m);
                list.add(s);
            }
            return list;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // returns a session identified by sid from a cinema identified by cid
    public static Session selectSession(Connection connection, int cid, int sid) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM SESSION AS s, CINEMA AS c, THEATER t, MOVIE as m " +
                    "WHERE s.ID = ? AND s.CinemaID = ? AND c.ID = s.CinemaID AND s.TheaterID = t.ID AND s.MovieID = m.ID");
            stmt.setInt(1, sid);
            stmt.setInt(2, cid);
            rs = stmt.executeQuery();
            rs.next();
            Theater t = TheaterDB.selectTheater(connection, cid, rs.getInt("TheaterID"));
            Movie m = MovieDB.selectMovie(connection, rs.getInt("MovieID"));
            return new Session(sid, toLocalDateTime(rs.getString("DateTime")), t, m);
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // returns a session identified by sid from a theater identified by tid from a cinema identified by cid
    public static Session selectSession(Connection connection, int cid, int tid, int sid) throws DBException{
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM SESSION AS s, CINEMA AS c, THEATER t, MOVIE as m " +
                    "WHERE s.ID = ? AND s.CinemaID = ? AND c.ID = s.CinemaID AND s.TheaterID = ? AND s.MovieID = m.ID");
            stmt.setInt(1, sid);
            stmt.setInt(2, cid);
            stmt.setInt(3, tid);
            rs = stmt.executeQuery();
            rs.next();
            Theater t = TheaterDB.selectTheater(connection, cid, tid);
            Movie m = MovieDB.selectMovie(connection, rs.getInt("MovieID"));
            return new Session(sid, toLocalDateTime(rs.getString("DateTime")), t, m);
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // returns a list of session that have the movie identified by mid
    public static List<Session> selectMoviesSessions(Connection connection, int mid) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM SESSION AS s, CINEMA AS c, THEATER as t, MOVIE as m " +
                    "WHERE s.MovieID = ? AND s.MovieID = m.ID AND s.TheaterID = t.ID AND s.CinemaID = c.ID");
            stmt.setInt(1, mid);
            rs = stmt.executeQuery();
            List<Session> list = new ArrayList<>();
            while (rs.next()) {
                Cinema c = CinemaDB.selectCinema(connection, rs.getInt("CinemaID"));
                Theater t = TheaterDB.selectTheater(connection, c.getId(), rs.getByte("TheaterID"));
                Movie m = MovieDB.selectMovie(connection, rs.getInt("MovieID"));
                Session s = new Session(rs.getInt(1), toLocalDateTime(rs.getString("DateTime")), t, m);
                list.add(s);
            }
            return list;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // returns a list of today sessions from a cinema identified by cid
    public static List<Session> selectSessionsToday(Connection connection, int cid) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            String date = now().toString();
            date = date.substring(0,date.indexOf("T"));
            stmt = connection.prepareStatement("SELECT * FROM SESSION AS s, CINEMA AS c, THEATER t, MOVIE as m " +
                    "WHERE s.CinemaID = ? AND c.ID = s.CinemaID AND s.TheaterID = t.ID AND s.MovieID = m.ID AND s.DateTime LIKE '%" + date + "%'");
            stmt.setInt(1, cid);
            rs = stmt.executeQuery();
            List<Session> list = new ArrayList<>();
            Cinema c = CinemaDB.selectCinema(connection, cid);
            while (rs.next()) {
                Theater t = TheaterDB.selectTheater(connection, cid, rs.getInt("TheaterID"));
                Movie m = MovieDB.selectMovie(connection, rs.getInt("MovieID"));
                Session s = new Session(rs.getInt("ID"), toLocalDateTime(rs.getString("DateTime")), t, m);
                list.add(s);
            }
            return list;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // returns a list of today session from a theater identified by tid and a cinema identified by cid
    public static List<Session> selectSessionsToday(Connection connection, int cid, int tid) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            String date = now().toString();
            date = date.substring(0,date.indexOf("T"));
            stmt = connection.prepareStatement("SELECT * FROM SESSION AS s, CINEMA AS c, THEATER t, MOVIE as m " +
                    "WHERE s.TheaterID = ? AND s.CinemaID = ? AND c.ID = s.CinemaID AND t.ID = S.TheaterID AND s.MovieID = m.ID AND" +
                    " s.DateTime LIKE '%" + date + "%'");

            stmt.setInt(1, tid);
            stmt.setInt(2, cid);
            rs = stmt.executeQuery();
            List<Session> list = new ArrayList<>();
            Theater t = TheaterDB.selectTheater(connection, cid, tid);
            while (rs.next()) {
                Movie m = MovieDB.selectMovie(connection, rs.getInt("MovieID"));
                Session s = new Session(rs.getInt("ID"), toLocalDateTime(rs.getString("DateTime")), t, m);
                list.add(s);
            }
            return list;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // returns a list of sessions from a date and a cinema identified by cid
    public static List<Session> selectSessionsDate(Connection connection, int cid, String date) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM SESSION AS s, CINEMA AS c, THEATER t, MOVIE as m " +
                    "WHERE s.CinemaID = ? AND c.ID = s.CinemaID AND s.TheaterID = t.ID AND s.MovieID = m.ID AND s.DateTime LIKE '%" + date + "%'");
            stmt.setInt(1, cid);
            rs = stmt.executeQuery();
            List<Session> list = new ArrayList<>();
            Cinema c = CinemaDB.selectCinema(connection, cid);
            while (rs.next()) {
                Theater t = TheaterDB.selectTheater(connection, c.getId(), rs.getInt("TheaterID"));
                Movie m = MovieDB.selectMovie(connection, rs.getInt("MovieID"));
                Session s = new Session(rs.getInt("ID"), toLocalDateTime(rs.getString("DateTime")), t, m);
                list.add(s);
            }
            return list;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // returns a list of sessions that have the movie identified by mid from a date and an optional parameter
    public static List<Session> selectSessionsDate(Connection connection, int mid, String date, String[] params) throws DBException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM SESSION AS s, CINEMA AS c, THEATER t, MOVIE as m" +
                    " WHERE s.CinemaID = c.ID and s.TheaterID = t.ID AND s.MovieID = m.ID AND m.ID = ? AND s.DateTime LIKE '%" + date + "%'");
            stmt.setInt(1, mid);
            rs = stmt.executeQuery();
            List<Session> sessions = new ArrayList<>();
            while (rs.next()) {
                Cinema c = CinemaDB.selectCinema(connection, rs.getInt("CinemaID"));
                Theater t = TheaterDB.selectTheater(connection, c.getId(), rs.getInt("TheaterID"));
                Movie m = MovieDB.selectMovie(connection, rs.getInt("MovieID"));
                Session s = new Session(rs.getInt("ID"), toLocalDateTime(rs.getString("DateTime")), t, m);
                sessions.add(s);
            }
            List<Session> list = new ArrayList<>();
            for (Session s : sessions) {
                switch (params[0]) {
                    case "city":
                        if (s.getTheater().getCinema().getCity().equals(params[1])) list.add(s);
                        break;
                    case "cid":
                        if (s.getTheater().getCinema().getId() == parseInt(params[1])) list.add(s);
                        break;
                    case "available":
                        int totalSeats = s.getTheater().getSeats();
                        stmt = connection.prepareStatement("SELECT COUNT(ID) FROM TICKET WHERE SessionID = ?");
                        stmt.setInt(1, s.getId());
                        rs = stmt.executeQuery();
                        rs.next();
                        int numberOfSeatsOccupied = rs.getInt(1);
                        int numberOfAvailableSeats = totalSeats - numberOfSeatsOccupied;
                        if (parseInt(params[1]) <= numberOfAvailableSeats) list.add(s);
                        break;
                    default: list.add(s);
                }
            }
            return list;
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

    // inserts a session s
    public static int insertSession(Connection connection, Session s) throws DBException, SessionOverlapException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM SESSION WHERE THEATERID = ? AND CINEMAID = ?");
            stmt.setInt(1, s.getTheater().getId());
            stmt.setInt(2, s.getTheater().getCinema().getId());
            rs = stmt.executeQuery();
            while (rs.next()) {
                Movie m = MovieDB.selectMovie(connection, rs.getInt(5));
                LocalDateTime ldt = toLocalDateTime(rs.getString("DateTime"));
                if (s.getDateTime().equals(ldt)
                        ||(s.getDateTime().isAfter(ldt) && s.getDateTime().isBefore(ldt.plusMinutes(m.getRuntime())))
                        || (ldt.isAfter(s.getDateTime()) && ldt.isBefore(s.getDateTime().plusMinutes(m.getRuntime())))) {
                    throw new SessionOverlapException("Cannot post session in that date/time because it is overlapping another session from that theater");
                }
            }
            stmt = connection.prepareStatement("INSERT INTO SESSION (DateTime, CinemaID, TheaterID, MovieID) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, s.getDateTime().toString().replace("T", ","));
            stmt.setInt(2, s.getTheater().getCinema().getId());
            stmt.setInt(3,s.getTheater().getId());
            stmt.setInt(4, s.getMovie().getId());
            stmt.executeUpdate();
            return getGeneratedID(stmt);
        }
        catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        finally {
            try {
                if (stmt != null) stmt.close();
                if (rs != null) rs.close();
            }
            catch (SQLException e) {
                throw new DBException(e.getMessage(), e);
            }
        }
    }

}






