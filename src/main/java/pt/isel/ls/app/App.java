package pt.isel.ls.app;

import pt.isel.ls.commands.Command;
import pt.isel.ls.exceptions.*;
import pt.isel.ls.results.Result;
import pt.isel.ls.utils.Pair;
import pt.isel.ls.commands.NoHeaderRequired;
import pt.isel.ls.view.View;
import pt.isel.ls.headers.Header;

import java.util.HashMap;
import java.util.Scanner;

import static pt.isel.ls.commands.CommandManager.addCommands;
import static pt.isel.ls.commands.CommandManager.getCommandAndValues;
import static pt.isel.ls.headers.HeaderManager.addHeaders;
import static pt.isel.ls.headers.HeaderManager.decodeHeader;
import static pt.isel.ls.view.View.*;
import static pt.isel.ls.view.View.getView;

public class App {

    public static void main(String[] args) {
        addCommands();
        addHeaders();
        addViews();
        if (args.length != 0) {
            execute(args);
        }
        else {
            System.out.println("|INTERACTIVE MODE ENTERED|");
            Scanner s = new Scanner(System.in);
            String line;
            while (true) {
                line = s.nextLine();
                args = line.split(" ");
                execute(args);
            }
        }
    }

    private static void execute(String[] args) {
        int METHOD_INDEX = 0;
        int PATH_INDEX = 1;
        int HEADER_INDEX = 2;
        int PARAMETERS_INDEX = 3;
        try {
            Pair<Command, HashMap<String, String>> pair = getCommandAndValues(args[METHOD_INDEX], args[PATH_INDEX]);
            Command command = pair.getFirst();
            HashMap<String, String> pathValues = pair.getSecond();
            Result rs;
            Header header;
            if (args.length == 2) {
                rs =  command.execute(pathValues, "");
                header = decodeHeader("");
            }
            else if (args.length == 3){
                if ((header = decodeHeader(args[HEADER_INDEX])) == null) {
                    rs = command.execute(pathValues, args[PARAMETERS_INDEX - 1]);
                    header = decodeHeader("");
                }
                else rs = command.execute(pathValues, args[PARAMETERS_INDEX - 1]);
            }
            else {
                if ((header = decodeHeader(args[HEADER_INDEX])) == null) {
                    throw new UnknownHeaderException("header incorrect");
                }
                rs = command.execute(pathValues, args[PARAMETERS_INDEX]);
            }
            if (rs != null && !(rs instanceof NoHeaderRequired)) {
                View v = getView(rs, header);
                v.output(v.getContent(), header);
            }
        }
        catch (CommandNotAvailableException | InternalErrorException | SessionOverlapException | WrongParametersException | UnknownHeaderException | DBException e) {
            System.out.println(e.getMessage());
        }
        catch (Exception e) {
            System.out.println("Something wrong happened:\n " + e);
        }
    }

}