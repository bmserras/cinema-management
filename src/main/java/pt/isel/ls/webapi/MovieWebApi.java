package pt.isel.ls.webapi;

import com.google.gson.Gson;
import pt.isel.ls.entities.Movie;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.http.IRequest;

import java.io.InputStream;
import java.io.InputStreamReader;

import static java.text.MessageFormat.format;

public class MovieWebApi {

    private static final String MOVIE_DB_HOST = "https://api.themoviedb.org/3/";
    private static final String MOVIE_DB_MOVIE = "movie/{1}?api_key={0}";
    private static final String API_KEY = "342cf845f10cc533b73dde560021141b";
    private final IRequest req;

    public MovieWebApi(IRequest req) {
        this.req = req;
    }

    /**
     * E.g. https://api.themoviedb.org/3/movie/860?api_key=***************
     */
    public Movie getMovie(int id) throws DBException {
        String ID = id + "";
        String moviedto = format(MOVIE_DB_MOVIE,API_KEY,ID);
        String format = MOVIE_DB_HOST + moviedto;
        InputStream stream = req.getBody(format);
        Gson gson = new Gson();
        return gson.fromJson(new InputStreamReader(stream),Movie.class);
    }
}
