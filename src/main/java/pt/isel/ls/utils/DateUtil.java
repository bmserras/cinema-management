package pt.isel.ls.utils;

import java.time.LocalDateTime;

import static java.lang.Integer.*;

public class DateUtil {

    public static LocalDateTime toLocalDateTime(String dateTime) {
        String[] dateTimeStr = dateTime.split(",");
        String[] dateStr = dateTimeStr[0].split("-");
        String[] timeStr = dateTimeStr[1].split(":");
        return LocalDateTime.of(
                parseInt(dateStr[0]),
                parseInt(dateStr[1]),
                parseInt(dateStr[2]),
                parseInt(timeStr[0]),
                parseInt(timeStr[1]))
                ;
    }

}
