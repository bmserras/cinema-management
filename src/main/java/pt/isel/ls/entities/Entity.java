package pt.isel.ls.entities;

import pt.isel.ls.utils.Pair;

import java.util.List;

public abstract class Entity {

    public abstract List<Pair<String, String>> toListDate();
    public abstract List<Pair<String, String>> toListDetailed();
    public abstract int getId();
}
