package pt.isel.ls.entities;

import pt.isel.ls.utils.Pair;

import java.util.ArrayList;
import java.util.List;

public class Movie extends Entity {

    private int id;
    private String title;
    private String release_date;
    private int runtime;

    public Movie(int id, String title, String releaseDate, int runtime) {
        this.id = id;
        this.title = title;
        this.release_date = releaseDate.substring(0, releaseDate.indexOf("-"));
        this.runtime = runtime;
    }

    public Movie(int id, String title, int releaseDate, int runtime) {
        this.id = id;
        this.title = title;
        this.release_date = "" + releaseDate;
        this.runtime = runtime;
    }

    public Movie(String title, int releaseDate, int runtime) {
        this.title = title;
        this.release_date = "" + releaseDate;
        this.runtime = runtime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public String getRelease_date() {
        return release_date;
    }

    public int getRuntime() {
        return runtime;
    }

    public List<Pair<String, String>> toListDate() {
        List<Pair<String, String>> list = new ArrayList<>();
        list.add(new Pair<>("ID", "" + id));
        list.add(new Pair<>("Title", title));
        return list;
    }

    public List<Pair<String, String>> toListDetailed() {
        List<Pair<String, String>> list = this.toListDate();
        list.add(new Pair<>("Release Year", "" + release_date));
        list.add(new Pair<>("Duration", "" + runtime));
        return list;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Movie other = (Movie) obj;
        return id == other.id && title.equals(other.title) && release_date == other.release_date && runtime == other.runtime;
    }

}
