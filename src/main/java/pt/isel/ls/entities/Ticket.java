package pt.isel.ls.entities;

import pt.isel.ls.utils.Pair;

import java.util.ArrayList;
import java.util.List;

public class Ticket extends Entity {

    private int id;
    private String row;
    private int seat;
    private Session session;

    public Ticket(int id, String row, int seat, Session session) {
        this.id = id;
        this.row = row;
        this.seat = seat;
        this.session = session;
    }

    public Ticket(String row, int seat, Session session) {
        this.row = row;
        this.seat = seat;
        this.session = session;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRow() {
        return row;
    }

    public int getSeat() {
        return seat;
    }

    public Session getSession() {
        return session;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Ticket other = (Ticket) obj;
        return id == other.id && row.equals(other.row) && seat == other.seat &&
                session.equals(other.session);
    }

    public List<Pair<String, String>> toListDate() {
        List<Pair<String, String>> list = new ArrayList<>();
        list.add(new Pair<>("ID", "" + id));
        list.add(new Pair<>("Row", row));
        list.add(new Pair<>("Seat", "" + seat));
        return list;
    }

    public List<Pair<String, String>> toListDetailed() {
        List<Pair<String, String>> list = this.toListDate();
        list.add(new Pair<>("Session", session.getDateTime().toString().replace("T", ",")));
        list.add(new Pair<>("Movie", session.getMovie().getTitle()));
        list.add(new Pair<>("Cinema", session.getTheater().getCinema().getName()));
        list.add(new Pair<>("Theater", session.getTheater().getName()));
        return list;
    }

}
