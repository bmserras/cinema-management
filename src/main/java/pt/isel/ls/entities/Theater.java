package pt.isel.ls.entities;

import pt.isel.ls.utils.Pair;

import java.util.ArrayList;
import java.util.List;

public class Theater extends Entity {

    private int id;
    private String name;
    private int seats;
    private int rows;
    private int seatsPerRow;
    private Cinema cinema;

    public Theater(int id, String name, int seats, int rows, int seatsPerRow, pt.isel.ls.entities.Cinema cinema) {
        this.id = id;
        this.name = name;
        this.seats = seats;
        this.rows = rows;
        this.seatsPerRow = seatsPerRow;
        this.cinema = cinema;
    }

    public Theater(String name, int seats, int rows, int seatsPerRow, pt.isel.ls.entities.Cinema cinema) {
        this.name = name;
        this.seats = seats;
        this.rows = rows;
        this.seatsPerRow = seatsPerRow;
        this.cinema = cinema;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getSeats() {
        return seats;
    }

    public int getRows() {
        return rows;
    }

    public int getSeatsPerRow() {
        return seatsPerRow;
    }

    public Cinema getCinema() {
        return cinema;
    }

    public List<Pair<String, String>> toListDate() {
        List<Pair<String, String>> list = new ArrayList<>();
        list.add(new Pair<>("ID", "" + id));
        list.add(new Pair<>("Name", name));
        return list;
    }

    public List<Pair<String, String>> toListDetailed() {
        List<Pair<String, String>> list = this.toListDate();
        list.add(new Pair<>("Cinema ", cinema.getName()));
        list.add(new Pair<>("Seats", "" + seats));
        list.add(new Pair<>("Rows", "" + rows));
        list.add(new Pair<>("Seats per row", "" + seatsPerRow));
        return list;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Theater other = (Theater) obj;
        return id == other.id && name.equals(other.name) && seats == other.seats &&
                rows == other.rows && seatsPerRow == other.seatsPerRow && cinema.equals(other.cinema);
    }

}
