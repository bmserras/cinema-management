package pt.isel.ls.entities;

import pt.isel.ls.utils.Pair;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Session extends Entity {

    private int id;
    private LocalDateTime dateTime;
    private Theater theater;
    private Movie movie;

    public Session(int id, LocalDateTime dateTime, Theater theater, Movie movie) {
        this.id = id;
        this.dateTime = dateTime;
        this.theater = theater;
        this.movie = movie;
    }

    public Session(LocalDateTime dateTime, Theater theater, Movie movie) {
        this.dateTime = dateTime;
        this.theater = theater;
        this.movie = movie;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Theater getTheater() {
        return theater;
    }

    public Movie getMovie() {
        return movie;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public List<Pair<String, String>> toListDate() {
        List<Pair<String, String>> list = new ArrayList<>();
        list.add(new Pair<>("ID", "" + id));
        list.add(new Pair<>("Date", dateTime.toString().replace("T", ",")));
        list.add(new Pair<>("Cinema", theater.getCinema().getName()));
        list.add(new Pair<>("Theater", theater.getName()));
        return list;
    }

    public List<Pair<String, String>> toListMovie() {
        List<Pair<String, String>> list = new ArrayList<>();
        list.add(new Pair<>("ID", "" + id));
        list.add(new Pair<>("Date", dateTime.toString().replace("T", ",")));
        list.add(new Pair<>("Movie", movie.getTitle()));
        return list;
    }

    public List<Pair<String, String>> toListTheater() {
        List<Pair<String, String>> list = new ArrayList<>();
        list.add(new Pair<>("ID", "" + id));
        list.add(new Pair<>("Date", dateTime.toString().replace("T", ",")));
        list.add(new Pair<>("Movie", movie.getTitle()));
        list.add(new Pair<>("Theater", theater.getName()));
        return list;
    }

    public List<Pair<String, String>> toListDetailed() {
        List<Pair<String, String>> list = this.toListDate();
        list.add(new Pair<>("Movie", movie.getTitle()));
        return list;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Session other = (Session) obj;
        return id == other.id && theater.equals(other.theater) && movie.equals(other.movie);
    }

}
