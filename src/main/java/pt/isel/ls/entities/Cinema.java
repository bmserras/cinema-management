package pt.isel.ls.entities;

import pt.isel.ls.utils.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.parseInt;

public class Cinema extends Entity {

    private int id;
    private String name;
    private String city;

    public Cinema(int id, String name, String city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }

    public Cinema(String name, String city) {
        this.name = name;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getCity() { return city; }

    public List<Pair<String, String>> toListDate() {
        List<Pair<String, String>> list = new ArrayList<>();
        list.add(new Pair<>("ID", "" + id));
        list.add(new Pair<>("Name", name));
        return list;
    }

    public List<Pair<String, String>> toListDetailed() {
        List<Pair<String, String>> list = this.toListDate();
        list.add(new Pair<>("City", city));
        return list;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Cinema other = (Cinema) obj;
        boolean b = id == other.id && name.equals(other.name) && city.equals(other.city);
        return b;
    }

}

