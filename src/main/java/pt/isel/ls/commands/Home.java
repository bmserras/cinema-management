package pt.isel.ls.commands;

import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultEmpty;

import java.util.HashMap;

public class Home implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters){
        return new ResultEmpty();
    }

    @Override
    public String commandDetails() {
        return "home page of the http server";
    }

}
