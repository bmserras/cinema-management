package pt.isel.ls.commands.delete;

import pt.isel.ls.commands.Command;
import pt.isel.ls.db.Connect;
import pt.isel.ls.db.EntityDB;
import pt.isel.ls.db.SessionDB;
import pt.isel.ls.db.TicketDB;
import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.exceptions.WrongParametersException;
import pt.isel.ls.results.Result;

import java.util.HashMap;

public class DeleteTickets implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException, WrongParametersException {
        Connect c = null;
        try {
            c = new Connect();
            String[] names = new String[]{"tkid"};
            String[] ticketList = EntityDB.separateParameters(names, parameters);
            int count = 0;
            Session s = SessionDB.selectSession(c.getConnection(), Integer.parseInt(pathValues.get("{cid}")), Integer.parseInt(pathValues.get("{sid}")));
            for (String ticket : ticketList) {
                count += TicketDB.deleteTicket(c.getConnection(), s,ticket.charAt(0)+"",Integer.parseInt(ticket.substring(1)));
            }
            System.out.println(count + " tickets deleted.");
            return null;
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "removes a ticket set, given the following parameter that can occur multiple times:\n -tkid";
    }

}