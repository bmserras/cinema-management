package pt.isel.ls.commands.get;

import pt.isel.ls.db.Connect;
import pt.isel.ls.entities.Cinema;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.commands.Command;
import pt.isel.ls.db.CinemaDB;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemas;

import java.util.HashMap;
import java.util.List;

public class GetCinemas implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException {
        Connect c = null;
        try {
            c = new Connect();
            List<Cinema> cinemas = CinemaDB.selectCinemas(c.getConnection());
            return new ResultCinemas(cinemas);
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "returns a list with all the cinemas";
    }

}
