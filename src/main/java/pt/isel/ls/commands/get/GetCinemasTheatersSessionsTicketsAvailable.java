package pt.isel.ls.commands.get;

import pt.isel.ls.commands.Command;

import pt.isel.ls.db.Connect;
import pt.isel.ls.db.SessionDB;
import pt.isel.ls.db.TicketDB;
import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.DBException;

import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasTheatersSessionsTicketsAvailable;

import java.util.HashMap;

import static java.lang.Integer.parseInt;

public class GetCinemasTheatersSessionsTicketsAvailable implements Command {

    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException {
        Connect c = null;
        try {
            c = new Connect();
            Session s = SessionDB.selectSession(c.getConnection(), parseInt(pathValues.get("{cid}")), parseInt(pathValues.get("{tid}")), parseInt(pathValues.get("{sid}")));
            int numberOfTickets = TicketDB.getNumberOfTickets(c.getConnection(), s.getId());
            return new ResultCinemasTheatersSessionsTicketsAvailable(s, numberOfTickets);
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "returns the number of available tickets for a session";
    }

}
