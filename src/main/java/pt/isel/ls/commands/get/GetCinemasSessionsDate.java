package pt.isel.ls.commands.get;

import pt.isel.ls.commands.Command;
import pt.isel.ls.db.CinemaDB;
import pt.isel.ls.db.Connect;
import pt.isel.ls.db.SessionDB;
import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasSessionsDate;
import pt.isel.ls.utils.DateUtil;

import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.*;
import static pt.isel.ls.utils.DateUtil.toLocalDateTime;

public class GetCinemasSessionsDate implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException {
        Connect c = null;
        try {
            c = new Connect();
            List<Session> list = SessionDB.selectSessionsDate(c.getConnection(), parseInt(pathValues.get("{cid}")), pathValues.get("{dmy}"));
            Cinema cinema = CinemaDB.selectCinema(c.getConnection(), parseInt(pathValues.get("{cid}")));
            return new ResultCinemasSessionsDate(list, cinema, toLocalDateTime(pathValues.get("{dmy}") + ",00:00"));
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "returns a list with the sessions in cinema cid in the date dmy, where dmy contains day, month and year\n Date format: YYYY-MM-DD";
    }

}
