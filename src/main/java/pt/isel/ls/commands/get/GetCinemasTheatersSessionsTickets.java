package pt.isel.ls.commands.get;

import pt.isel.ls.commands.Command;

import pt.isel.ls.db.Connect;
import pt.isel.ls.db.TicketDB;
import pt.isel.ls.entities.Ticket;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasTheatersSessionsTickets;

import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.parseInt;


public class GetCinemasTheatersSessionsTickets implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues,String parameters) throws DBException {
        Connect c = null;
        try {
            c = new Connect();
            List<Ticket> list = TicketDB.selectTickets(c.getConnection(), parseInt(pathValues.get("{cid}")), parseInt(pathValues.get("{tid}")), parseInt(pathValues.get("{sid}")));;
            return new ResultCinemasTheatersSessionsTickets(list);
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "returns a list with all tickets for a session";
    }

}
