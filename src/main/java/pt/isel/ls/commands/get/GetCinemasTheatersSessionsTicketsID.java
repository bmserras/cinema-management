package pt.isel.ls.commands.get;

import pt.isel.ls.commands.Command;
import pt.isel.ls.db.Connect;
import pt.isel.ls.db.SessionDB;
import pt.isel.ls.db.TicketDB;
import pt.isel.ls.entities.Session;
import pt.isel.ls.entities.Ticket;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasTheatersSessionsTicketsID;

import java.util.HashMap;

import static java.lang.Integer.parseInt;

public class GetCinemasTheatersSessionsTicketsID implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException {
        Connect c = null;
        try {
            c = new Connect();
            Ticket ticket = TicketDB.selectTicket(c.getConnection(), parseInt(pathValues.get("{cid}")), parseInt(pathValues.get("{tid}")),
                    parseInt(pathValues.get("{sid}")), parseInt(pathValues.get("{tkid}")));
            return new ResultCinemasTheatersSessionsTicketsID(ticket, ticket.getSession());
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "returns the detailed information of the ticket, including session information";
    }

}
