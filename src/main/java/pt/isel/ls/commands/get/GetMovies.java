package pt.isel.ls.commands.get;

import pt.isel.ls.commands.Command;

import pt.isel.ls.db.Connect;
import pt.isel.ls.db.MovieDB;
import pt.isel.ls.entities.Movie;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultMovies;

import java.util.HashMap;
import java.util.List;

public class GetMovies implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException {
        Connect c = null;
        try {
            c = new Connect();
            List<Movie> list = MovieDB.selectMovies(c.getConnection());
            return new ResultMovies(list);
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "returns a list with all movies";
    }

}



