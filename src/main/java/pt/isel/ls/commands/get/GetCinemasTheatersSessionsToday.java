package pt.isel.ls.commands.get;

import pt.isel.ls.commands.Command;
import pt.isel.ls.db.Connect;
import pt.isel.ls.db.SessionDB;
import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasTheatersSessionsToday;


import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.parseInt;

public class GetCinemasTheatersSessionsToday implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException {
        Connect c = null;
        try {
            c = new Connect();
            List<Session> list = SessionDB.selectSessionsToday(c.getConnection(), parseInt(pathValues.get("{cid}")), parseInt(pathValues.get("{tid}")));
            return new ResultCinemasTheatersSessionsToday(list);
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }


    @Override
    public String commandDetails() {
        return "returns a list with all sessions for the current day in the cinema and theater identified by cid and tid respectively";
    }

}
