package pt.isel.ls.commands.get;

import pt.isel.ls.commands.Command;
import pt.isel.ls.db.Connect;
import pt.isel.ls.db.SessionDB;
import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasSessionsID;

import java.util.HashMap;

import static java.lang.Integer.parseInt;

public class GetCinemasSessionsID implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException {
        Connect c = null;
        try {
            c = new Connect();
            Session session = SessionDB.selectSession(c.getConnection(), parseInt(pathValues.get("{cid}")), parseInt(pathValues.get("{sid}")));
            return new ResultCinemasSessionsID(session);
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "returns the detailed information for the session identified by sid";
    }

}
