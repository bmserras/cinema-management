package pt.isel.ls.commands.get;

import pt.isel.ls.commands.Command;
import pt.isel.ls.db.Connect;
import pt.isel.ls.db.SessionDB;
import pt.isel.ls.db.TheaterDB;
import pt.isel.ls.entities.Session;
import pt.isel.ls.entities.Theater;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasTheatersID;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.parseInt;

public class GetCinemasTheatersID implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException {
        Connect c = null;
        try {
            c = new Connect();
            Theater theater = TheaterDB.selectTheater(c.getConnection(), parseInt(pathValues.get("{cid}")), parseInt(pathValues.get("{tid}")));
            List<Session> sessions = SessionDB.selectSessions(c.getConnection(), Integer.parseInt(pathValues.get("{cid}")), Integer.parseInt(pathValues.get("{tid}")));
            return new ResultCinemasTheatersID(sessions,theater);
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "returns the detailed information for the theater identified by tid";
    }

}
