package pt.isel.ls.commands.get;

import pt.isel.ls.db.Connect;
import pt.isel.ls.db.MovieDB;
import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Movie;
import pt.isel.ls.entities.Theater;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.commands.Command;
import pt.isel.ls.db.CinemaDB;
import pt.isel.ls.db.TheaterDB;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasID;

import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.*;

public class GetCinemasID implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException {
        Connect c = null;
        try {
            c = new Connect();
            Cinema cinema = CinemaDB.selectCinema(c.getConnection(), parseInt(pathValues.get("{cid}")));
            List<Theater> theaters = TheaterDB.selectTheaters(c.getConnection(), cinema.getId());
            List<Movie> movies = MovieDB.selectCinemasMovies(c.getConnection(), cinema.getId());
            return new ResultCinemasID(cinema, theaters, movies);
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "returns the detailed information for the cinema identified by cid";
    }

}
