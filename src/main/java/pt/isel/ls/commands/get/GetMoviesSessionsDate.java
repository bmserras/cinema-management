package pt.isel.ls.commands.get;

import pt.isel.ls.commands.Command;
import pt.isel.ls.db.Connect;
import pt.isel.ls.db.EntityDB;
import pt.isel.ls.db.SessionDB;
import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.exceptions.WrongParametersException;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultMoviesSessionsDate;

import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.parseInt;

public class GetMoviesSessionsDate implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException, WrongParametersException {
        Connect c = null;
        try {
            c = new Connect();
            String[] names = new String[]{"city", "cid", "available"};
            String[] params = EntityDB.checkOptionalParameter(names, parameters);
            List<Session> list = SessionDB.selectSessionsDate(c.getConnection(), parseInt(pathValues.get("{mid}")), pathValues.get("{dmy}"), params);
            return new ResultMoviesSessionsDate(list);
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "returns a list with the sessions for the movie identified by mid in the day of the year dmy given one of the following optional parameters:\n" +
                " -city\n -cid\n -available\n Date format: YYYY-MM-DD";
    }

}
