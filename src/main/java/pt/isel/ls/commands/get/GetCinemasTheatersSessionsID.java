package pt.isel.ls.commands.get;

import pt.isel.ls.commands.Command;
import pt.isel.ls.db.*;
import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Session;
import pt.isel.ls.entities.Theater;
import pt.isel.ls.entities.Ticket;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasTheatersSessionsID;

import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.*;
import static java.lang.Integer.parseInt;

public class GetCinemasTheatersSessionsID implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException {
        Connect c = null;
        try {
            c = new Connect();
            Session session = SessionDB.selectSession(c.getConnection(), parseInt(pathValues.get("{cid}")), parseInt(pathValues.get("{tid}")), parseInt(pathValues.get("{sid}")));
            List<Ticket> tickets = TicketDB.selectTickets(c.getConnection(), parseInt(pathValues.get("{cid}")), parseInt(pathValues.get("{tid}")), parseInt(pathValues.get("{sid}")));
            return new ResultCinemasTheatersSessionsID(session.getTheater().getCinema() ,session.getTheater(), tickets , session);
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return null;
    }

}
