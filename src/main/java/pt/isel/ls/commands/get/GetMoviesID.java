package pt.isel.ls.commands.get;

import pt.isel.ls.commands.Command;
import pt.isel.ls.db.CinemaDB;
import pt.isel.ls.db.Connect;
import pt.isel.ls.db.MovieDB;
import pt.isel.ls.db.SessionDB;
import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Movie;
import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultMoviesID;


import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.*;

public class GetMoviesID implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues,String parameters) throws DBException {
        Connect c = null;
        try {
            c = new Connect();
            Movie m = MovieDB.selectMovie(c.getConnection(), parseInt(pathValues.get("{mid}")));
            List<Session> sessions =  SessionDB.selectMoviesSessions(c.getConnection(), parseInt(pathValues.get("{mid}")));
            List<Cinema> cinemas = CinemaDB.selectMoviesCinemas(c.getConnection(), parseInt(pathValues.get("{mid}")));
            return new ResultMoviesID(m, cinemas, sessions);
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "returns a list with the sessions for the movie identified by mid in the day of the year dmy given one of the following optional parameters";
    }

}
