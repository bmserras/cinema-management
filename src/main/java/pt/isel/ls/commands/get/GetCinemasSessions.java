package pt.isel.ls.commands.get;

import pt.isel.ls.commands.Command;
import pt.isel.ls.db.Connect;
import pt.isel.ls.db.SessionDB;
import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasSessions;

import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.parseInt;

public class GetCinemasSessions implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues,String parameters) throws DBException {
        Connect c = null;
        try {
            c = new Connect();
            List<Session> list = SessionDB.selectSessions(c.getConnection(), parseInt(pathValues.get("{cid}")));
            return new ResultCinemasSessions(list);
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "returns a list with all sessions in cinema identified by cid";
    }

}