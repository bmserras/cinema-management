package pt.isel.ls.commands.get;

import pt.isel.ls.commands.Command;

import pt.isel.ls.db.CinemaDB;
import pt.isel.ls.db.Connect;
import pt.isel.ls.db.SessionDB;
import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Session;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultCinemasSessionsDateToday;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.*;
import static java.time.LocalDateTime.*;

public class GetCinemasSessionsDateToday implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException {
        Connect c = null;
        try {
            c = new Connect();
            List<Session> list = SessionDB.selectSessionsToday(c.getConnection(), parseInt(pathValues.get("{cid}")));
            Cinema cinema = CinemaDB.selectCinema(c.getConnection(), parseInt(pathValues.get("{cid}")));
            return new ResultCinemasSessionsDateToday(list, cinema, now());
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "returns a list with all sessions for the current day in the cinema identified by cid";
    }

}
