package pt.isel.ls.commands.post;

import pt.isel.ls.commands.Command;
import pt.isel.ls.db.CinemaDB;
import pt.isel.ls.db.Connect;
import pt.isel.ls.db.EntityDB;
import pt.isel.ls.db.TheaterDB;
import pt.isel.ls.entities.Cinema;
import pt.isel.ls.entities.Theater;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.exceptions.WrongParametersException;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultPost;

import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.Integer.parseInt;

public class PostTheaters implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException, WrongParametersException {
        Connect c = null;
        try {
            c = new Connect();
            ArrayList<String> names = new ArrayList<>();
            names.add("name");
            names.add("rows");
            names.add("seats");
            HashMap<String, String> params = EntityDB.separateParameters(names, parameters);
            Cinema cinema = CinemaDB.selectCinema(c.getConnection(), parseInt(pathValues.get("{cid}")));
            Theater t = new Theater(params.get("name"), parseInt(params.get("rows")) * parseInt(params.get("seats")),
                    parseInt(params.get("rows")), parseInt(params.get("seats")), cinema);
            t.setId(TheaterDB.insertTheater(c.getConnection(), t));
            System.out.println("The Theater has been posted with the ID number " + t.getId());
            return new ResultPost(t);
        }
        catch (NumberFormatException e) {
            throw new WrongParametersException("Row or seat must be a number", e);
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "Posts a theater given the following parameters:\n -name\n -rows\n -seats";
    }

}
