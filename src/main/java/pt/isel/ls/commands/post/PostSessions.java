package pt.isel.ls.commands.post;

import pt.isel.ls.commands.Command;
import pt.isel.ls.db.*;
import pt.isel.ls.entities.Movie;
import pt.isel.ls.entities.Session;
import pt.isel.ls.entities.Theater;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.exceptions.SessionOverlapException;
import pt.isel.ls.exceptions.WrongParametersException;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultPost;

import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.Integer.parseInt;
import static pt.isel.ls.utils.DateUtil.toLocalDateTime;

public class PostSessions implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException, WrongParametersException, SessionOverlapException {
        Connect c = null;
        try {
            c = new Connect();
            ArrayList<String> names = new ArrayList<>();
            names.add("date");
            names.add("mid");
            HashMap<String, String> params = EntityDB.separateParameters(names, parameters);
            Theater t = TheaterDB.selectTheater(c.getConnection(), parseInt(pathValues.get("{cid}")), parseInt(pathValues.get("{tid}")));
            Movie m = MovieDB.selectMovie(c.getConnection(), parseInt(params.get("mid")));
            Session s = new Session(toLocalDateTime(params.get("date")), t, m);
            s.setId(SessionDB.insertSession(c.getConnection(), s));
            System.out.println("The session has been posted with the ID number " + s.getId());
            return new ResultPost(s);
        }
        catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            throw new WrongParametersException("Incorrect date format, must be YYYY-MM-DD,HH:MM", e);
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "Posts a session given the following parameters:\n -date\n -mid\n Date format: YYYY-MM-DD,HH:MM";
    }

}
