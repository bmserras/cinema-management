package pt.isel.ls.commands.post;

import pt.isel.ls.commands.Command;
import pt.isel.ls.db.CinemaDB;
import pt.isel.ls.db.Connect;
import pt.isel.ls.db.EntityDB;
import pt.isel.ls.entities.Cinema;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.exceptions.WrongParametersException;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultPost;

import java.util.ArrayList;
import java.util.HashMap;

public class PostCinemas implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException, WrongParametersException {
        Connect c = null;
        try {
            c = new Connect();
            ArrayList<String> names = new ArrayList<>();
            names.add("name");
            names.add("city");
            HashMap<String, String> params = EntityDB.separateParameters(names, parameters);
            Cinema cinema = new Cinema(params.get("name"), params.get("city"));
            cinema.setId(CinemaDB.insertCinema(c.getConnection(), cinema));
            System.out.println("The Cinema has been posted with the ID number " + cinema.getId());
            return new ResultPost(cinema);
        }
        finally {
            if (c != null) c.closeConnection();
        }

    }

    @Override
    public String commandDetails() {
        return "creates a new cinema, given the following parameters:\n -name\n -city";
    }

}
