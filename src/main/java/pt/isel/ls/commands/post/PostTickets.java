package pt.isel.ls.commands.post;

import pt.isel.ls.commands.Command;
import pt.isel.ls.db.Connect;
import pt.isel.ls.db.EntityDB;
import pt.isel.ls.db.SessionDB;
import pt.isel.ls.db.TicketDB;
import pt.isel.ls.entities.Session;
import pt.isel.ls.entities.Ticket;
import pt.isel.ls.exceptions.*;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultPost;

import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.Integer.*;

public class PostTickets implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws DBException, WrongParametersException, AlreadyExistsRowSeatException, TicketNotAvailableException, NotBetweenAandZException {
        Connect c = null;
        try {
            c = new Connect();
            ArrayList<String> names = new ArrayList<>();
            names.add("row");
            names.add("seat");
            HashMap<String, String> params = EntityDB.separateParameters(names, parameters);
            Session s = SessionDB.selectSession(c.getConnection(), parseInt(pathValues.get("{cid}")), parseInt(pathValues.get("{sid}")));
            Ticket t = new Ticket(params.get("row"), parseInt(params.get("seat")), s);
            if(!(params.get("row").charAt(0)<= 'Z' && params.get("row").charAt(0) >= 'A')) {
                throw new NotBetweenAandZException(params.get("row") + " is not between A and Z");
            }
            if (params.get("row").charAt(0) > 'A' + s.getTheater().getRows() - 1)
                throw new TicketNotAvailableException("That row/seat is not available");
            if (TicketDB.selectTicketByRowSeat(c.getConnection(), parseInt(pathValues.get("{cid}")),
                    parseInt(pathValues.get("{tid}")),
                    parseInt(pathValues.get("{sid}")),
                    params.get("row"), parseInt(params.get("seat"))) != null) {
                throw new AlreadyExistsRowSeatException("The seat " + params.get("row") + params.get("seat") + " is already occupied");
            }
            int numberOfTickets = TicketDB.getNumberOfTickets(c.getConnection(), s.getId());
            if (numberOfTickets >= s.getTheater().getSeats()) {
                throw new TicketNotAvailableException("There are no more seats available");
            }
            t.setId(TicketDB.insertTicket(c.getConnection(), t));
            System.out.println("The ticket has been posted with the ID number " + t.getId());
            return new ResultPost(t);
        }
        catch (NumberFormatException e) {
            throw new WrongParametersException("Make sure the row is a number and the seat is a letter");
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "Posts a new ticket given the following parameters:\n -row\n - seat";
    }

}
