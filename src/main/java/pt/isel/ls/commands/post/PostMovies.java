package pt.isel.ls.commands.post;

import pt.isel.ls.commands.Command;
import pt.isel.ls.db.Connect;
import pt.isel.ls.db.EntityDB;
import pt.isel.ls.db.MovieDB;
import pt.isel.ls.entities.Movie;
import pt.isel.ls.exceptions.DBException;
import pt.isel.ls.exceptions.InternalErrorException;
import pt.isel.ls.exceptions.WrongParametersException;
import pt.isel.ls.http.HttpRequest;
import pt.isel.ls.http.IRequest;
import pt.isel.ls.webapi.MovieWebApi;
import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultPost;

import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.Integer.parseInt;

public class PostMovies implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws WrongParametersException, DBException {
        Connect c = null;
        try {
            c = new Connect();
            IRequest req = new HttpRequest();
            MovieDB db = new MovieDB(new MovieWebApi(req));
            ArrayList<String> names = new ArrayList<>();
            names.add("id");
            HashMap<String, String> params = EntityDB.separateParameters(names, parameters);
            Movie m = db.getWebApi().getMovie(parseInt(params.get("id")));
            m.setId(MovieDB.insertMovie(c.getConnection(), m));
            System.out.println("The Movie has been posted with the ID number " + m.getId());
            return new ResultPost(m);
        }
        finally {
            if (c != null) c.closeConnection();
        }
    }

    @Override
    public String commandDetails() {
        return "Posts a new movie, given the following parameters:\n -title\n -releaseYear\n -duration";
    }

}
