package pt.isel.ls.commands;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import pt.isel.ls.http.AppServlet;
import pt.isel.ls.results.Result;

import java.util.HashMap;

public class Listen implements Command {

    private static final int LISTEN_PORT = 8080;

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) throws Exception {
        String portParam = null;
        String paramName = parameters.split("=")[0];
        if (paramName.equals("port")) {
            portParam = parameters.split("=")[1];
        }
        String portDef = System.getenv("PORT");
        int port = portDef != null ? Integer.valueOf(portDef) : portParam != null ? Integer.parseInt(portParam) : LISTEN_PORT;
        System.out.println("Listening on port " + port);
        Server server = new Server(port);
        ServletHandler handler = new ServletHandler();
        server.setHandler(handler);
        handler.addServletWithMapping(new ServletHolder(new AppServlet()), "/*");
        server.start();
        return null;
    }

    @Override
    public String commandDetails() {
        return "Opens a local server given the optional parameter:\n -port";
    }

}
