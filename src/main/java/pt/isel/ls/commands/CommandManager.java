package pt.isel.ls.commands;

import pt.isel.ls.commands.delete.DeleteTickets;
import pt.isel.ls.commands.get.*;
import pt.isel.ls.commands.post.*;
import pt.isel.ls.exceptions.CommandNotAvailableException;
import pt.isel.ls.utils.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommandManager {

    public static HashMap<String, Command> commandList;

    private static HashMap<String, String> entitiesIds;
    private static List<String> entities;
    private static List<String> functions;

    public static void addCommands() {

        entitiesIds = new HashMap<>();
        entitiesIds.put("cinemas", "{cid}");
        entitiesIds.put("theaters", "{tid}");
        entitiesIds.put("sessions", "{sid}");
        entitiesIds.put("tickets", "{tkid}");
        entitiesIds.put("movies", "{mid}");
        entitiesIds.put("date", "{dmy}");

        entities = new ArrayList<>();
        entities.add("cinemas");
        entities.add("theaters");
        entities.add("sessions");
        entities.add("tickets");
        entities.add("movies");
        entities.add("date");

        functions = new ArrayList<>();
        functions.add("today");
        functions.add("available");

        commandList = new HashMap<>();

        commandList.put("GET /",new Home());

        commandList.put("GET /cinemas",new GetCinemas());
        commandList.put("GET /cinemas/{cid}",new GetCinemasID());

        commandList.put("GET /movies",new GetMovies());
        commandList.put("GET /movies/{mid}",new GetMoviesID());

        commandList.put("GET /cinemas/{cid}/theaters",new GetCinemasTheaters());
        commandList.put("GET /cinemas/{cid}/theaters/{tid}",new GetCinemasTheatersID());

        commandList.put("GET /cinemas/{cid}/sessions",new GetCinemasSessions());
        commandList.put("GET /cinemas/{cid}/theaters/{tid}/sessions",new GetCinemasTheatersSessions());
        commandList.put("GET /cinemas/{cid}/theaters/{tid}/sessions/{sid}",new GetCinemasTheatersSessionsID());
        commandList.put("GET /cinemas/{cid}/sessions/{sid}",new GetCinemasSessionsID());

        commandList.put("GET /cinemas/{cid}/sessions/today",new GetCinemasSessionsToday());
        commandList.put("GET /cinemas/{cid}/sessions/date/today",new GetCinemasSessionsDateToday());
        commandList.put("GET /cinemas/{cid}/theaters/{tid}/sessions/today",new GetCinemasTheatersSessionsToday());
        commandList.put("GET /cinemas/{cid}/sessions/date/{dmy}",new GetCinemasSessionsDate());
        commandList.put("GET /movies/{mid}/sessions/date/{dmy}",new GetMoviesSessionsDate());

        commandList.put("GET /cinemas/{cid}/theaters/{tid}/sessions/{sid}/tickets",new GetCinemasTheatersSessionsTickets());
        commandList.put("GET /cinemas/{cid}/theaters/{tid}/sessions/{sid}/tickets/{tkid}",new GetCinemasTheatersSessionsTicketsID());
        commandList.put("GET /cinemas/{cid}/theaters/{tid}/sessions/{sid}/tickets/available",new GetCinemasTheatersSessionsTicketsAvailable());


        commandList.put("DELETE /cinemas/{cid}/theaters/{tid}/sessions/{sid}/tickets",new DeleteTickets());

        commandList.put("POST /movies",new PostMovies());
        commandList.put("POST /cinemas",new PostCinemas());
        commandList.put("POST /cinemas/{cid}/theaters",new PostTheaters());
        commandList.put("POST /cinemas/{cid}/theaters/{tid}/sessions",new PostSessions());
        commandList.put("POST /cinemas/{cid}/theaters/{tid}/sessions/{sid}/tickets",new PostTickets());

        commandList.put("OPTION /", new Option());
        commandList.put("EXIT /", new Exit());

        commandList.put("LISTEN /", new Listen());

    }

    public static Pair<Command, HashMap<String, String>>  getCommandAndValues(String method, String path) throws CommandNotAvailableException {
        HashMap<String, String> values = new HashMap<>();
        StringBuilder key = new StringBuilder(method + " ");
        String entity = "";
        String[] pathParts = path.split("/");
        key.append(pathParts.length == 0 ? "/" : "");
        for (int i = 1; i < pathParts.length; i++) {
            if (entities.contains(pathParts[i]) || functions.contains(pathParts[i])) {
                entity = pathParts[i];
                key.append("/").append(entity);
            } else {
                String replacement = entitiesIds.get(entity);
                key.append("/").append(replacement);
                values.put(replacement,pathParts[i]);
            }
        }
        Command c = commandList.get(key.toString());
        if (c == null) {
            throw new CommandNotAvailableException("Command not available, type ' OPTION / ' for a list of available commands");
        }
        return new Pair<>(c, values);
    }


}
