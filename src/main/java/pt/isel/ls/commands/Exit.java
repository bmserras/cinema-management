package pt.isel.ls.commands;

import pt.isel.ls.db.ExitDB;
import pt.isel.ls.results.Result;

import java.util.HashMap;

public class Exit implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) {
        ExitDB.exit();
        return null;
    }

    @Override
    public String commandDetails() {
        return "ends the application";
    }

}
