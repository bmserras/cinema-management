package pt.isel.ls.commands;

import pt.isel.ls.results.Result;

import java.util.HashMap;

public interface Command {

    Result execute(HashMap<String, String> pathValues, String parameters) throws Exception;

    String commandDetails();

}
