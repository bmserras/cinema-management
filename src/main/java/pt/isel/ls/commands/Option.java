package pt.isel.ls.commands;

import pt.isel.ls.results.Result;
import pt.isel.ls.results.ResultOption;
import pt.isel.ls.utils.Pair;

import java.util.*;

import static pt.isel.ls.commands.CommandManager.commandList;

public class Option implements Command {

    @Override
    public Result execute(HashMap<String, String> pathValues, String parameters) {
        List<Pair<String, String>> list = new ArrayList<>();
        Set<Map.Entry<String, Command>> entries = commandList.entrySet();
        for (Map.Entry<String,Command> p : entries) {
            list.add(new Pair<>(p.getKey() + " ", p.getValue().commandDetails()));
        }
        return new ResultOption(list);
    }

    @Override
    public String commandDetails() {
        return "presents a list of available commands and their characteristics";
    }

}
