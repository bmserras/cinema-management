# Projeto de LS do Grupo 8 da turma 41D do Semestre de Verão
# Cinema Management App <a href="#"><img src="https://img.shields.io/badge/Version-1.0.0-brightgreen.svg?&style=flat-square"></a>  <a href="https://github.com/isel-leic-ls/1718-1-LI41D-G8/wiki"><img src="https://img.shields.io/badge/Wiki-Home-red.svg?style=flat-square"></a>

Uma aplicação para Gestão de Cinemas.

## Objetivo desta Aplicação

Confere a gestão de cinemas, salas, sessões, bilhetes e filmes.
Utiliza também a API pública [MovieDB](https://www.themoviedb.org/) para a obtenção de filmes.

## Heroku App

Pode executar a aplicação através do [Heroku](https://isel-ls-1718-2-li41d-g8.herokuapp.com/).